grammar MathExpr;

options {
  language=CSharp2;
  output=AST;
  ASTLabelType = AstNode;

  backtrack=true;
  memoize=true;
}


tokens {
  UNKNOWN             ;
  BLOCK               ;
  PARAMS              ;
  ARGUMENTS           ;
  CALL                ;
  VAR                 ;
  VAR_ARRAY  		  ;
  ARRAY_INDEX		  ;
  ARRAY_CALL		  ;
  ARRAY_SET		  	  ;
  ARRAY_INIT		  ;
  IF      = 'if'      ;
  ELSE    = 'else'    ;
  FOR     = 'for'     ;
  WHILE   = 'while'   ;
  DO      = 'do'      ;
  RETURN  = 'return'  ;
  FUNCTION  = 'function'  ;
  PROGRAM   = 'program'   ;
  TRUE    = 'true'        ;
  FALSE   = 'false'       ;
}


@lexer::namespace { MathExpr }
@parser::namespace { MathExpr }


WS:
  ( ' ' | '\t' | '\f' | '\r' | '\n' )+ {
    $channel=HIDDEN;
  }
;


ML_COMMENT:
  '/*' ( options { greedy=false; } : . )* '*/' {
    $channel=HIDDEN;
  }
;


NUMBER: ('0'..'9')+ ('.' ('0'..'9')+)?
;
IDENT:  ( 'a'..'z' | 'A'..'Z' | '_' )
        ( 'a'..'z' | 'A'..'Z' | '_' | '0'..'9' )*
;
ADD:     '+'    ;
SUB:     '-'    ;
MUL:     '*'    ;
DIV:     '/'    ;
MOD:     '%'    ;
XOR:     '^'    ;
AND:     '&&'   ;
OR:      '||'   ;
NOT:     '!'    ;
BIT_AND: '&'    ;
BIT_OR:  '|'    ;
ASSIGN : '='
	| '*='
	| '/='
	| '%='
	| '+='
	| '-='
	| '<<='
	| '>>='
	| '&='
	| '^='
	| '|='
	;
UNARY : '++'
	| '--'
	;
GE:       '>='  ;
LE:       '<='  ;
NEQUALS:  '!='  ;
EQUALS:   '=='   ;
GT:       '>'   ;
LT:       '<'   ;

ident: IDENT;

type: IDENT^;

params_: ( term (','! term)* )?  ;
call: ident '(' params_ ')'  -> ^(CALL ident ^(PARAMS params_?)) ;


callArray: ident arrayIndex  -> ^(ARRAY_CALL ident arrayIndex) ;

group:
  '('! term ')'!
| NUMBER
| TRUE
| FALSE
| ident
| call
| callArray
;

not:   group | NOT^ not ;
mult:  not ( ( MUL | DIV | BIT_AND | MOD | XOR )^ not )*  ;
add:   mult  ( ( ADD | SUB | BIT_OR )^ mult  )*                   ;
compare: add ( ( GE | LE | NEQUALS | EQUALS | GT | LT )^ add )?   ;
and_logic: compare ( AND^ compare )*    ;
or_logic: and_logic ( OR^ and_logic )*  ;
term: or_logic  ;


arrayConst: '['! NUMBER ']'! | '['! NUMBER ']'! arrayConst;

arrayNumber: NUMBER | NUMBER ','! arrayNumber;
arrayInit: '{'! arrayNumber '}'!;

varDecl: ident (ASSIGN^ term)?;

varsDecl: type varDecl ( ',' varDecl )* -> ^(VAR ^(type varDecl+)) 
        | type arrayConst varDecl ( ',' varDecl )* -> ^(VAR_ARRAY ^(type arrayConst) varDecl+)
		| type arrayConst ident ASSIGN arrayInit -> ^(ASSIGN ^(VAR_ARRAY ^(type arrayConst) ident) ^(ARRAY_INIT arrayInit+));

arrayIndex: 
  ('['! term ']'!)+ 
;

assign0:
ident arrayIndex -> ^(ARRAY_SET ident ^(ARRAY_INDEX arrayIndex))
| ident
;

expr0:
  assign0 ASSIGN^ term
| ident UNARY^
| UNARY^ ident
| ident -> ^(CALL ident PARAMS)
| call
| varsDecl
;
blockExpr: '{'! exprList '}'! ;

termOrTrue: 
  term
| ( ) -> TRUE
;

expr:
  expr0 ';'!
| IF^ '('! term ')'! expr (ELSE! expr)?
| WHILE^ '('! term ')'! expr
| FOR^ '('! exprList2 ';'! termOrTrue ';'! exprList2 ')'! expr
| DO^ expr WHILE! '('! term ')'!
| RETURN^ term ';'!
| call ';'!
| callArray ';'!
| blockExpr
;
  
exprList: ( expr ( ';'* expr )* )? ';'*  ->  ^(BLOCK expr*)  ;

exprList2: ( expr0 ( ',' expr0 )* )?  ->  ^(BLOCK expr0*)  ;

paramDecl: type IDENT^ ;

paramsDecl: paramDecl ( ','! paramDecl )* ;

funcDecl:
  t=type n=ident '(' paramsDecl? ')'
  '{' exprList '}'
  -> ^(FUNCTION $t $n ^(ARGUMENTS paramsDecl*) exprList)
;

exprOrFuncDecl: funcDecl | expr ;

program: ( exprOrFuncDecl ';'!* )* ;

result: program -> ^(PROGRAM program) ;

execute:
  result
;
