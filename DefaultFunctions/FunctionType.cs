﻿using System;
using System.Collections.Generic;

namespace MathExpr
{
    public enum FunctionResultType
    {
        PUSH = 0,
        POP = 1
    }
    public class FunctionType
    {
        String name;
        String systemName;
        String type;
        String[] args;
        FunctionResultType resultType;

        public FunctionType(String name, String systemName, String type, String[] args)
        {
            this.name = name;
            this.systemName = systemName;
            this.type = type;
            this.args = args;
            resultType = type == "void" ? FunctionResultType.PUSH : FunctionResultType.POP;
        }

        public FunctionType(String name, String systemName, String type, Variable[] args)
        {
            this.name = name;
            this.systemName = systemName;
            this.type = type;
            String[] ars = new String[args.Length];
            for (int i = 0; i < args.Length; i++)
                ars[i] = args[i].Type;
            this.args = ars;
            resultType = type == "void" ? FunctionResultType.PUSH : FunctionResultType.POP;
        }

        public String Name { get {return name;} }
        public String SystemName { get { return systemName; } }
        public String Type { get { return type; } }
        public String[] Arguments { get { return args; } }
        public override string ToString()
        {
            return type + " " + systemName + "(" + argsToString() + ")";
        }

        public FunctionResultType ResultType { get { return resultType; } }

        public bool IsPopType()
        {
            return resultType == FunctionResultType.POP;
        }
        public String argsToString()
        {
            if (args.Length == 0) return "";
            if (args.Length == 1) return args[0];
            String res = "";
            for (int i=0; i<args.Length; i++)
            {
                if (i!= args.Length-1)
                    res += args[i] + ",";
                else
                    res += args[i];
            }
            return res;

        }
    }
}
