﻿using System;
using System.Collections.Generic;

namespace MathExpr
{
    public class DefaultFunctions
    {
        private static DefaultFunctions instance = new DefaultFunctions();

        private List<FunctionType> defaultFunctions = new List<FunctionType>();

        private void Initialize()
        {
            defaultFunctions.Add(new FunctionType("printf", "[mscorlib]System.Console::WriteLine", "void", new String[] { "int32" }));
            defaultFunctions.Add(new FunctionType("scanf", "[mscorlib]System.Console::Read", "int32", new String[] {}));
        }

        private DefaultFunctions()
        {
            Initialize();
        }

        public static DefaultFunctions INSTANCE
        {
            get
            {
                if (instance == null)
                    instance = new DefaultFunctions();
                return instance;
            }
        }
        
        public FunctionType search(String name)
        {
            foreach (var f in defaultFunctions)
            {
                if (f.Name == name) return f;
            }
            return null;
        }

        public bool isDefault(String name)
        {
            return search(name) != null;
        }
    }
}
