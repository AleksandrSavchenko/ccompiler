﻿using System;
using System.Collections.Generic;

namespace MathExpr
{
    public class Variable
    {
        private String name;
        private String type;

        public Variable(String name, String type)
        {
            this.name = name;
            this.type = TypeConverter.Convert(type);
        }

        public String Name { get { return this.name; } }
        public String Type { get { return this.type; } }

        public override bool Equals(Object v)
        {
            Variable var = v as Variable;
            return var.name == this.name && var.type == this.type;
        }
    }
}
