// $ANTLR 3.2 Sep 23, 2009 12:02:23 MathExpr.g 2015-12-22 23:07:09

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;


namespace  MathExpr 
{
public partial class MathExprLexer : Lexer {
    public const int FUNCTION = 21;
    public const int LT = 47;
    public const int WHILE = 18;
    public const int BIT_AND = 38;
    public const int MOD = 33;
    public const int DO = 19;
    public const int FOR = 17;
    public const int SUB = 30;
    public const int EQUALS = 45;
    public const int NOT = 37;
    public const int AND = 35;
    public const int EOF = -1;
    public const int NEQUALS = 44;
    public const int IF = 15;
    public const int T__55 = 55;
    public const int ML_COMMENT = 26;
    public const int T__51 = 51;
    public const int T__52 = 52;
    public const int ARRAY_SET = 13;
    public const int T__53 = 53;
    public const int BIT_OR = 39;
    public const int UNKNOWN = 4;
    public const int T__54 = 54;
    public const int ARRAY_INDEX = 11;
    public const int RETURN = 20;
    public const int IDENT = 28;
    public const int ARRAY_CALL = 12;
    public const int VAR = 9;
    public const int UNARY = 41;
    public const int ARRAY_INIT = 14;
    public const int T__50 = 50;
    public const int PARAMS = 6;
    public const int ADD = 29;
    public const int GE = 42;
    public const int XOR = 34;
    public const int ARGUMENTS = 7;
    public const int T__48 = 48;
    public const int T__49 = 49;
    public const int ELSE = 16;
    public const int NUMBER = 27;
    public const int MUL = 31;
    public const int TRUE = 23;
    public const int VAR_ARRAY = 10;
    public const int WS = 25;
    public const int BLOCK = 5;
    public const int OR = 36;
    public const int ASSIGN = 40;
    public const int GT = 46;
    public const int PROGRAM = 22;
    public const int CALL = 8;
    public const int DIV = 32;
    public const int FALSE = 24;
    public const int LE = 43;

    // delegates
    // delegators

    public MathExprLexer() 
    {
		InitializeCyclicDFAs();
    }
    public MathExprLexer(ICharStream input)
		: this(input, null) {
    }
    public MathExprLexer(ICharStream input, RecognizerSharedState state)
		: base(input, state) {
		InitializeCyclicDFAs(); 

    }
    
    override public string GrammarFileName
    {
    	get { return "MathExpr.g";} 
    }

    // $ANTLR start "IF"
    public void mIF() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = IF;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:9:4: ( 'if' )
            // MathExpr.g:9:6: 'if'
            {
            	Match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "IF"

    // $ANTLR start "ELSE"
    public void mELSE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ELSE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:10:6: ( 'else' )
            // MathExpr.g:10:8: 'else'
            {
            	Match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ELSE"

    // $ANTLR start "FOR"
    public void mFOR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = FOR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:11:5: ( 'for' )
            // MathExpr.g:11:7: 'for'
            {
            	Match("for"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "FOR"

    // $ANTLR start "WHILE"
    public void mWHILE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = WHILE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:12:7: ( 'while' )
            // MathExpr.g:12:9: 'while'
            {
            	Match("while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "WHILE"

    // $ANTLR start "DO"
    public void mDO() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = DO;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:13:4: ( 'do' )
            // MathExpr.g:13:6: 'do'
            {
            	Match("do"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "DO"

    // $ANTLR start "RETURN"
    public void mRETURN() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = RETURN;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:14:8: ( 'return' )
            // MathExpr.g:14:10: 'return'
            {
            	Match("return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "RETURN"

    // $ANTLR start "FUNCTION"
    public void mFUNCTION() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = FUNCTION;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:15:10: ( 'function' )
            // MathExpr.g:15:12: 'function'
            {
            	Match("function"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "FUNCTION"

    // $ANTLR start "PROGRAM"
    public void mPROGRAM() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = PROGRAM;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:16:9: ( 'program' )
            // MathExpr.g:16:11: 'program'
            {
            	Match("program"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "PROGRAM"

    // $ANTLR start "TRUE"
    public void mTRUE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = TRUE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:17:6: ( 'true' )
            // MathExpr.g:17:8: 'true'
            {
            	Match("true"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "TRUE"

    // $ANTLR start "FALSE"
    public void mFALSE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = FALSE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:18:7: ( 'false' )
            // MathExpr.g:18:9: 'false'
            {
            	Match("false"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "FALSE"

    // $ANTLR start "T__48"
    public void mT__48() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__48;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:19:7: ( ',' )
            // MathExpr.g:19:9: ','
            {
            	Match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public void mT__49() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__49;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:20:7: ( '(' )
            // MathExpr.g:20:9: '('
            {
            	Match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public void mT__50() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__50;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:21:7: ( ')' )
            // MathExpr.g:21:9: ')'
            {
            	Match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public void mT__51() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__51;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:22:7: ( '[' )
            // MathExpr.g:22:9: '['
            {
            	Match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public void mT__52() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__52;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:23:7: ( ']' )
            // MathExpr.g:23:9: ']'
            {
            	Match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public void mT__53() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__53;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:24:7: ( '{' )
            // MathExpr.g:24:9: '{'
            {
            	Match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public void mT__54() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__54;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:25:7: ( '}' )
            // MathExpr.g:25:9: '}'
            {
            	Match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public void mT__55() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__55;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:26:7: ( ';' )
            // MathExpr.g:26:9: ';'
            {
            	Match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "WS"
    public void mWS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = WS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:42:3: ( ( ' ' | '\\t' | '\\f' | '\\r' | '\\n' )+ )
            // MathExpr.g:43:3: ( ' ' | '\\t' | '\\f' | '\\r' | '\\n' )+
            {
            	// MathExpr.g:43:3: ( ' ' | '\\t' | '\\f' | '\\r' | '\\n' )+
            	int cnt1 = 0;
            	do 
            	{
            	    int alt1 = 2;
            	    int LA1_0 = input.LA(1);

            	    if ( ((LA1_0 >= '\t' && LA1_0 <= '\n') || (LA1_0 >= '\f' && LA1_0 <= '\r') || LA1_0 == ' ') )
            	    {
            	        alt1 = 1;
            	    }


            	    switch (alt1) 
            		{
            			case 1 :
            			    // MathExpr.g:
            			    {
            			    	if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n') || (input.LA(1) >= '\f' && input.LA(1) <= '\r') || input.LA(1) == ' ' ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}


            			    }
            			    break;

            			default:
            			    if ( cnt1 >= 1 ) goto loop1;
            		            EarlyExitException eee1 =
            		                new EarlyExitException(1, input);
            		            throw eee1;
            	    }
            	    cnt1++;
            	} while (true);

            	loop1:
            		;	// Stops C# compiler whining that label 'loop1' has no statements


            	    _channel=HIDDEN;
            	  

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "ML_COMMENT"
    public void mML_COMMENT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ML_COMMENT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:49:11: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // MathExpr.g:50:3: '/*' ( options {greedy=false; } : . )* '*/'
            {
            	Match("/*"); 

            	// MathExpr.g:50:8: ( options {greedy=false; } : . )*
            	do 
            	{
            	    int alt2 = 2;
            	    int LA2_0 = input.LA(1);

            	    if ( (LA2_0 == '*') )
            	    {
            	        int LA2_1 = input.LA(2);

            	        if ( (LA2_1 == '/') )
            	        {
            	            alt2 = 2;
            	        }
            	        else if ( ((LA2_1 >= '\u0000' && LA2_1 <= '.') || (LA2_1 >= '0' && LA2_1 <= '\uFFFF')) )
            	        {
            	            alt2 = 1;
            	        }


            	    }
            	    else if ( ((LA2_0 >= '\u0000' && LA2_0 <= ')') || (LA2_0 >= '+' && LA2_0 <= '\uFFFF')) )
            	    {
            	        alt2 = 1;
            	    }


            	    switch (alt2) 
            		{
            			case 1 :
            			    // MathExpr.g:50:38: .
            			    {
            			    	MatchAny(); 

            			    }
            			    break;

            			default:
            			    goto loop2;
            	    }
            	} while (true);

            	loop2:
            		;	// Stops C# compiler whining that label 'loop2' has no statements

            	Match("*/"); 


            	    _channel=HIDDEN;
            	  

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ML_COMMENT"

    // $ANTLR start "NUMBER"
    public void mNUMBER() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NUMBER;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:56:7: ( ( '0' .. '9' )+ ( '.' ( '0' .. '9' )+ )? )
            // MathExpr.g:56:9: ( '0' .. '9' )+ ( '.' ( '0' .. '9' )+ )?
            {
            	// MathExpr.g:56:9: ( '0' .. '9' )+
            	int cnt3 = 0;
            	do 
            	{
            	    int alt3 = 2;
            	    int LA3_0 = input.LA(1);

            	    if ( ((LA3_0 >= '0' && LA3_0 <= '9')) )
            	    {
            	        alt3 = 1;
            	    }


            	    switch (alt3) 
            		{
            			case 1 :
            			    // MathExpr.g:56:10: '0' .. '9'
            			    {
            			    	MatchRange('0','9'); 

            			    }
            			    break;

            			default:
            			    if ( cnt3 >= 1 ) goto loop3;
            		            EarlyExitException eee3 =
            		                new EarlyExitException(3, input);
            		            throw eee3;
            	    }
            	    cnt3++;
            	} while (true);

            	loop3:
            		;	// Stops C# compiler whining that label 'loop3' has no statements

            	// MathExpr.g:56:21: ( '.' ( '0' .. '9' )+ )?
            	int alt5 = 2;
            	int LA5_0 = input.LA(1);

            	if ( (LA5_0 == '.') )
            	{
            	    alt5 = 1;
            	}
            	switch (alt5) 
            	{
            	    case 1 :
            	        // MathExpr.g:56:22: '.' ( '0' .. '9' )+
            	        {
            	        	Match('.'); 
            	        	// MathExpr.g:56:26: ( '0' .. '9' )+
            	        	int cnt4 = 0;
            	        	do 
            	        	{
            	        	    int alt4 = 2;
            	        	    int LA4_0 = input.LA(1);

            	        	    if ( ((LA4_0 >= '0' && LA4_0 <= '9')) )
            	        	    {
            	        	        alt4 = 1;
            	        	    }


            	        	    switch (alt4) 
            	        		{
            	        			case 1 :
            	        			    // MathExpr.g:56:27: '0' .. '9'
            	        			    {
            	        			    	MatchRange('0','9'); 

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt4 >= 1 ) goto loop4;
            	        		            EarlyExitException eee4 =
            	        		                new EarlyExitException(4, input);
            	        		            throw eee4;
            	        	    }
            	        	    cnt4++;
            	        	} while (true);

            	        	loop4:
            	        		;	// Stops C# compiler whining that label 'loop4' has no statements


            	        }
            	        break;

            	}


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NUMBER"

    // $ANTLR start "IDENT"
    public void mIDENT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = IDENT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:58:6: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // MathExpr.g:58:9: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            	if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || input.LA(1) == '_' || (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) 
            	{
            	    input.Consume();

            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    Recover(mse);
            	    throw mse;}

            	// MathExpr.g:59:9: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            	do 
            	{
            	    int alt6 = 2;
            	    int LA6_0 = input.LA(1);

            	    if ( ((LA6_0 >= '0' && LA6_0 <= '9') || (LA6_0 >= 'A' && LA6_0 <= 'Z') || LA6_0 == '_' || (LA6_0 >= 'a' && LA6_0 <= 'z')) )
            	    {
            	        alt6 = 1;
            	    }


            	    switch (alt6) 
            		{
            			case 1 :
            			    // MathExpr.g:
            			    {
            			    	if ( (input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || input.LA(1) == '_' || (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}


            			    }
            			    break;

            			default:
            			    goto loop6;
            	    }
            	} while (true);

            	loop6:
            		;	// Stops C# compiler whining that label 'loop6' has no statements


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "IDENT"

    // $ANTLR start "ADD"
    public void mADD() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ADD;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:61:4: ( '+' )
            // MathExpr.g:61:10: '+'
            {
            	Match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ADD"

    // $ANTLR start "SUB"
    public void mSUB() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = SUB;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:62:4: ( '-' )
            // MathExpr.g:62:10: '-'
            {
            	Match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "SUB"

    // $ANTLR start "MUL"
    public void mMUL() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = MUL;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:63:4: ( '*' )
            // MathExpr.g:63:10: '*'
            {
            	Match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "MUL"

    // $ANTLR start "DIV"
    public void mDIV() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = DIV;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:64:4: ( '/' )
            // MathExpr.g:64:10: '/'
            {
            	Match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "DIV"

    // $ANTLR start "MOD"
    public void mMOD() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = MOD;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:65:4: ( '%' )
            // MathExpr.g:65:10: '%'
            {
            	Match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "MOD"

    // $ANTLR start "XOR"
    public void mXOR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = XOR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:66:4: ( '^' )
            // MathExpr.g:66:10: '^'
            {
            	Match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "XOR"

    // $ANTLR start "AND"
    public void mAND() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = AND;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:67:4: ( '&&' )
            // MathExpr.g:67:10: '&&'
            {
            	Match("&&"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "OR"
    public void mOR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = OR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:68:3: ( '||' )
            // MathExpr.g:68:10: '||'
            {
            	Match("||"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "NOT"
    public void mNOT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NOT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:69:4: ( '!' )
            // MathExpr.g:69:10: '!'
            {
            	Match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NOT"

    // $ANTLR start "BIT_AND"
    public void mBIT_AND() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = BIT_AND;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:70:8: ( '&' )
            // MathExpr.g:70:10: '&'
            {
            	Match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "BIT_AND"

    // $ANTLR start "BIT_OR"
    public void mBIT_OR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = BIT_OR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:71:7: ( '|' )
            // MathExpr.g:71:10: '|'
            {
            	Match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "BIT_OR"

    // $ANTLR start "ASSIGN"
    public void mASSIGN() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ASSIGN;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:72:8: ( '=' | '*=' | '/=' | '%=' | '+=' | '-=' | '<<=' | '>>=' | '&=' | '^=' | '|=' )
            int alt7 = 11;
            switch ( input.LA(1) ) 
            {
            case '=':
            	{
                alt7 = 1;
                }
                break;
            case '*':
            	{
                alt7 = 2;
                }
                break;
            case '/':
            	{
                alt7 = 3;
                }
                break;
            case '%':
            	{
                alt7 = 4;
                }
                break;
            case '+':
            	{
                alt7 = 5;
                }
                break;
            case '-':
            	{
                alt7 = 6;
                }
                break;
            case '<':
            	{
                alt7 = 7;
                }
                break;
            case '>':
            	{
                alt7 = 8;
                }
                break;
            case '&':
            	{
                alt7 = 9;
                }
                break;
            case '^':
            	{
                alt7 = 10;
                }
                break;
            case '|':
            	{
                alt7 = 11;
                }
                break;
            	default:
            	    NoViableAltException nvae_d7s0 =
            	        new NoViableAltException("", 7, 0, input);

            	    throw nvae_d7s0;
            }

            switch (alt7) 
            {
                case 1 :
                    // MathExpr.g:72:10: '='
                    {
                    	Match('='); 

                    }
                    break;
                case 2 :
                    // MathExpr.g:73:4: '*='
                    {
                    	Match("*="); 


                    }
                    break;
                case 3 :
                    // MathExpr.g:74:4: '/='
                    {
                    	Match("/="); 


                    }
                    break;
                case 4 :
                    // MathExpr.g:75:4: '%='
                    {
                    	Match("%="); 


                    }
                    break;
                case 5 :
                    // MathExpr.g:76:4: '+='
                    {
                    	Match("+="); 


                    }
                    break;
                case 6 :
                    // MathExpr.g:77:4: '-='
                    {
                    	Match("-="); 


                    }
                    break;
                case 7 :
                    // MathExpr.g:78:4: '<<='
                    {
                    	Match("<<="); 


                    }
                    break;
                case 8 :
                    // MathExpr.g:79:4: '>>='
                    {
                    	Match(">>="); 


                    }
                    break;
                case 9 :
                    // MathExpr.g:80:4: '&='
                    {
                    	Match("&="); 


                    }
                    break;
                case 10 :
                    // MathExpr.g:81:4: '^='
                    {
                    	Match("^="); 


                    }
                    break;
                case 11 :
                    // MathExpr.g:82:4: '|='
                    {
                    	Match("|="); 


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ASSIGN"

    // $ANTLR start "UNARY"
    public void mUNARY() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = UNARY;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:84:7: ( '++' | '--' )
            int alt8 = 2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0 == '+') )
            {
                alt8 = 1;
            }
            else if ( (LA8_0 == '-') )
            {
                alt8 = 2;
            }
            else 
            {
                NoViableAltException nvae_d8s0 =
                    new NoViableAltException("", 8, 0, input);

                throw nvae_d8s0;
            }
            switch (alt8) 
            {
                case 1 :
                    // MathExpr.g:84:9: '++'
                    {
                    	Match("++"); 


                    }
                    break;
                case 2 :
                    // MathExpr.g:85:4: '--'
                    {
                    	Match("--"); 


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "UNARY"

    // $ANTLR start "GE"
    public void mGE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = GE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:87:3: ( '>=' )
            // MathExpr.g:87:11: '>='
            {
            	Match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "GE"

    // $ANTLR start "LE"
    public void mLE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:88:3: ( '<=' )
            // MathExpr.g:88:11: '<='
            {
            	Match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LE"

    // $ANTLR start "NEQUALS"
    public void mNEQUALS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NEQUALS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:89:8: ( '!=' )
            // MathExpr.g:89:11: '!='
            {
            	Match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NEQUALS"

    // $ANTLR start "EQUALS"
    public void mEQUALS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = EQUALS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:90:7: ( '==' )
            // MathExpr.g:90:11: '=='
            {
            	Match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "EQUALS"

    // $ANTLR start "GT"
    public void mGT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = GT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:91:3: ( '>' )
            // MathExpr.g:91:11: '>'
            {
            	Match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "LT"
    public void mLT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // MathExpr.g:92:3: ( '<' )
            // MathExpr.g:92:11: '<'
            {
            	Match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LT"

    override public void mTokens() // throws RecognitionException 
    {
        // MathExpr.g:1:8: ( IF | ELSE | FOR | WHILE | DO | RETURN | FUNCTION | PROGRAM | TRUE | FALSE | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | WS | ML_COMMENT | NUMBER | IDENT | ADD | SUB | MUL | DIV | MOD | XOR | AND | OR | NOT | BIT_AND | BIT_OR | ASSIGN | UNARY | GE | LE | NEQUALS | EQUALS | GT | LT )
        int alt9 = 41;
        alt9 = dfa9.Predict(input);
        switch (alt9) 
        {
            case 1 :
                // MathExpr.g:1:10: IF
                {
                	mIF(); 

                }
                break;
            case 2 :
                // MathExpr.g:1:13: ELSE
                {
                	mELSE(); 

                }
                break;
            case 3 :
                // MathExpr.g:1:18: FOR
                {
                	mFOR(); 

                }
                break;
            case 4 :
                // MathExpr.g:1:22: WHILE
                {
                	mWHILE(); 

                }
                break;
            case 5 :
                // MathExpr.g:1:28: DO
                {
                	mDO(); 

                }
                break;
            case 6 :
                // MathExpr.g:1:31: RETURN
                {
                	mRETURN(); 

                }
                break;
            case 7 :
                // MathExpr.g:1:38: FUNCTION
                {
                	mFUNCTION(); 

                }
                break;
            case 8 :
                // MathExpr.g:1:47: PROGRAM
                {
                	mPROGRAM(); 

                }
                break;
            case 9 :
                // MathExpr.g:1:55: TRUE
                {
                	mTRUE(); 

                }
                break;
            case 10 :
                // MathExpr.g:1:60: FALSE
                {
                	mFALSE(); 

                }
                break;
            case 11 :
                // MathExpr.g:1:66: T__48
                {
                	mT__48(); 

                }
                break;
            case 12 :
                // MathExpr.g:1:72: T__49
                {
                	mT__49(); 

                }
                break;
            case 13 :
                // MathExpr.g:1:78: T__50
                {
                	mT__50(); 

                }
                break;
            case 14 :
                // MathExpr.g:1:84: T__51
                {
                	mT__51(); 

                }
                break;
            case 15 :
                // MathExpr.g:1:90: T__52
                {
                	mT__52(); 

                }
                break;
            case 16 :
                // MathExpr.g:1:96: T__53
                {
                	mT__53(); 

                }
                break;
            case 17 :
                // MathExpr.g:1:102: T__54
                {
                	mT__54(); 

                }
                break;
            case 18 :
                // MathExpr.g:1:108: T__55
                {
                	mT__55(); 

                }
                break;
            case 19 :
                // MathExpr.g:1:114: WS
                {
                	mWS(); 

                }
                break;
            case 20 :
                // MathExpr.g:1:117: ML_COMMENT
                {
                	mML_COMMENT(); 

                }
                break;
            case 21 :
                // MathExpr.g:1:128: NUMBER
                {
                	mNUMBER(); 

                }
                break;
            case 22 :
                // MathExpr.g:1:135: IDENT
                {
                	mIDENT(); 

                }
                break;
            case 23 :
                // MathExpr.g:1:141: ADD
                {
                	mADD(); 

                }
                break;
            case 24 :
                // MathExpr.g:1:145: SUB
                {
                	mSUB(); 

                }
                break;
            case 25 :
                // MathExpr.g:1:149: MUL
                {
                	mMUL(); 

                }
                break;
            case 26 :
                // MathExpr.g:1:153: DIV
                {
                	mDIV(); 

                }
                break;
            case 27 :
                // MathExpr.g:1:157: MOD
                {
                	mMOD(); 

                }
                break;
            case 28 :
                // MathExpr.g:1:161: XOR
                {
                	mXOR(); 

                }
                break;
            case 29 :
                // MathExpr.g:1:165: AND
                {
                	mAND(); 

                }
                break;
            case 30 :
                // MathExpr.g:1:169: OR
                {
                	mOR(); 

                }
                break;
            case 31 :
                // MathExpr.g:1:172: NOT
                {
                	mNOT(); 

                }
                break;
            case 32 :
                // MathExpr.g:1:176: BIT_AND
                {
                	mBIT_AND(); 

                }
                break;
            case 33 :
                // MathExpr.g:1:184: BIT_OR
                {
                	mBIT_OR(); 

                }
                break;
            case 34 :
                // MathExpr.g:1:191: ASSIGN
                {
                	mASSIGN(); 

                }
                break;
            case 35 :
                // MathExpr.g:1:198: UNARY
                {
                	mUNARY(); 

                }
                break;
            case 36 :
                // MathExpr.g:1:204: GE
                {
                	mGE(); 

                }
                break;
            case 37 :
                // MathExpr.g:1:207: LE
                {
                	mLE(); 

                }
                break;
            case 38 :
                // MathExpr.g:1:210: NEQUALS
                {
                	mNEQUALS(); 

                }
                break;
            case 39 :
                // MathExpr.g:1:218: EQUALS
                {
                	mEQUALS(); 

                }
                break;
            case 40 :
                // MathExpr.g:1:225: GT
                {
                	mGT(); 

                }
                break;
            case 41 :
                // MathExpr.g:1:228: LT
                {
                	mLT(); 

                }
                break;

        }

    }


    protected DFA9 dfa9;
	private void InitializeCyclicDFAs()
	{
	    this.dfa9 = new DFA9(this);
	}

    const string DFA9_eotS =
        "\x01\uffff\x08\x14\x09\uffff\x01\x2c\x02\uffff\x01\x2e\x01\x2f"+
        "\x01\x30\x01\x31\x01\x32\x01\x34\x01\x36\x01\x38\x01\x2b\x01\x3b"+
        "\x01\x3d\x01\x3e\x05\x14\x01\x44\x03\x14\x15\uffff\x01\x14\x01\x49"+
        "\x03\x14\x01\uffff\x03\x14\x01\x50\x01\uffff\x05\x14\x01\x56\x01"+
        "\uffff\x01\x14\x01\x58\x01\x59\x02\x14\x01\uffff\x01\x14\x02\uffff"+
        "\x01\x5d\x02\x14\x01\uffff\x01\x60\x01\x61\x02\uffff";
    const string DFA9_eofS =
        "\x62\uffff";
    const string DFA9_minS =
        "\x01\x09\x01\x66\x01\x6c\x01\x61\x01\x68\x01\x6f\x01\x65\x02\x72"+
        "\x09\uffff\x01\x2a\x02\uffff\x01\x2b\x01\x2d\x03\x3d\x01\x26\x03"+
        "\x3d\x01\x3c\x01\x3d\x01\x30\x01\x73\x01\x72\x01\x6e\x01\x6c\x01"+
        "\x69\x01\x30\x01\x74\x01\x6f\x01\x75\x15\uffff\x01\x65\x01\x30\x01"+
        "\x63\x01\x73\x01\x6c\x01\uffff\x01\x75\x01\x67\x01\x65\x01\x30\x01"+
        "\uffff\x01\x74\x02\x65\x02\x72\x01\x30\x01\uffff\x01\x69\x02\x30"+
        "\x01\x6e\x01\x61\x01\uffff\x01\x6f\x02\uffff\x01\x30\x01\x6d\x01"+
        "\x6e\x01\uffff\x02\x30\x02\uffff";
    const string DFA9_maxS =
        "\x01\x7d\x01\x66\x01\x6c\x01\x75\x01\x68\x01\x6f\x01\x65\x02\x72"+
        "\x09\uffff\x01\x3d\x02\uffff\x06\x3d\x01\x7c\x03\x3d\x01\x3e\x01"+
        "\x7a\x01\x73\x01\x72\x01\x6e\x01\x6c\x01\x69\x01\x7a\x01\x74\x01"+
        "\x6f\x01\x75\x15\uffff\x01\x65\x01\x7a\x01\x63\x01\x73\x01\x6c\x01"+
        "\uffff\x01\x75\x01\x67\x01\x65\x01\x7a\x01\uffff\x01\x74\x02\x65"+
        "\x02\x72\x01\x7a\x01\uffff\x01\x69\x02\x7a\x01\x6e\x01\x61\x01\uffff"+
        "\x01\x6f\x02\uffff\x01\x7a\x01\x6d\x01\x6e\x01\uffff\x02\x7a\x02"+
        "\uffff";
    const string DFA9_acceptS =
        "\x09\uffff\x01\x0b\x01\x0c\x01\x0d\x01\x0e\x01\x0f\x01\x10\x01"+
        "\x11\x01\x12\x01\x13\x01\uffff\x01\x15\x01\x16\x15\uffff\x01\x14"+
        "\x01\x22\x01\x1a\x01\x23\x01\x17\x01\x18\x01\x19\x01\x1b\x01\x1c"+
        "\x01\x1d\x01\x20\x01\x1e\x01\x21\x01\x26\x01\x1f\x01\x27\x01\x25"+
        "\x01\x29\x01\x24\x01\x28\x01\x01\x05\uffff\x01\x05\x04\uffff\x01"+
        "\x03\x06\uffff\x01\x02\x05\uffff\x01\x09\x01\uffff\x01\x0a\x01\x04"+
        "\x03\uffff\x01\x06\x02\uffff\x01\x08\x01\x07";
    const string DFA9_specialS =
        "\x62\uffff}>";
    static readonly string[] DFA9_transitionS = {
            "\x02\x11\x01\uffff\x02\x11\x12\uffff\x01\x11\x01\x1c\x03\uffff"+
            "\x01\x18\x01\x1a\x01\uffff\x01\x0a\x01\x0b\x01\x17\x01\x15\x01"+
            "\x09\x01\x16\x01\uffff\x01\x12\x0a\x13\x01\uffff\x01\x10\x01"+
            "\x1e\x01\x1d\x01\x1f\x02\uffff\x1a\x14\x01\x0c\x01\uffff\x01"+
            "\x0d\x01\x19\x01\x14\x01\uffff\x03\x14\x01\x05\x01\x02\x01\x03"+
            "\x02\x14\x01\x01\x06\x14\x01\x07\x01\x14\x01\x06\x01\x14\x01"+
            "\x08\x02\x14\x01\x04\x03\x14\x01\x0e\x01\x1b\x01\x0f",
            "\x01\x20",
            "\x01\x21",
            "\x01\x24\x0d\uffff\x01\x22\x05\uffff\x01\x23",
            "\x01\x25",
            "\x01\x26",
            "\x01\x27",
            "\x01\x28",
            "\x01\x29",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\x01\x2a\x12\uffff\x01\x2b",
            "",
            "",
            "\x01\x2d\x11\uffff\x01\x2b",
            "\x01\x2d\x0f\uffff\x01\x2b",
            "\x01\x2b",
            "\x01\x2b",
            "\x01\x2b",
            "\x01\x33\x16\uffff\x01\x2b",
            "\x01\x2b\x3e\uffff\x01\x35",
            "\x01\x37",
            "\x01\x39",
            "\x01\x2b\x01\x3a",
            "\x01\x3c\x01\x2b",
            "\x0a\x14\x07\uffff\x1a\x14\x04\uffff\x01\x14\x01\uffff\x1a"+
            "\x14",
            "\x01\x3f",
            "\x01\x40",
            "\x01\x41",
            "\x01\x42",
            "\x01\x43",
            "\x0a\x14\x07\uffff\x1a\x14\x04\uffff\x01\x14\x01\uffff\x1a"+
            "\x14",
            "\x01\x45",
            "\x01\x46",
            "\x01\x47",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\x01\x48",
            "\x0a\x14\x07\uffff\x1a\x14\x04\uffff\x01\x14\x01\uffff\x1a"+
            "\x14",
            "\x01\x4a",
            "\x01\x4b",
            "\x01\x4c",
            "",
            "\x01\x4d",
            "\x01\x4e",
            "\x01\x4f",
            "\x0a\x14\x07\uffff\x1a\x14\x04\uffff\x01\x14\x01\uffff\x1a"+
            "\x14",
            "",
            "\x01\x51",
            "\x01\x52",
            "\x01\x53",
            "\x01\x54",
            "\x01\x55",
            "\x0a\x14\x07\uffff\x1a\x14\x04\uffff\x01\x14\x01\uffff\x1a"+
            "\x14",
            "",
            "\x01\x57",
            "\x0a\x14\x07\uffff\x1a\x14\x04\uffff\x01\x14\x01\uffff\x1a"+
            "\x14",
            "\x0a\x14\x07\uffff\x1a\x14\x04\uffff\x01\x14\x01\uffff\x1a"+
            "\x14",
            "\x01\x5a",
            "\x01\x5b",
            "",
            "\x01\x5c",
            "",
            "",
            "\x0a\x14\x07\uffff\x1a\x14\x04\uffff\x01\x14\x01\uffff\x1a"+
            "\x14",
            "\x01\x5e",
            "\x01\x5f",
            "",
            "\x0a\x14\x07\uffff\x1a\x14\x04\uffff\x01\x14\x01\uffff\x1a"+
            "\x14",
            "\x0a\x14\x07\uffff\x1a\x14\x04\uffff\x01\x14\x01\uffff\x1a"+
            "\x14",
            "",
            ""
    };

    static readonly short[] DFA9_eot = DFA.UnpackEncodedString(DFA9_eotS);
    static readonly short[] DFA9_eof = DFA.UnpackEncodedString(DFA9_eofS);
    static readonly char[] DFA9_min = DFA.UnpackEncodedStringToUnsignedChars(DFA9_minS);
    static readonly char[] DFA9_max = DFA.UnpackEncodedStringToUnsignedChars(DFA9_maxS);
    static readonly short[] DFA9_accept = DFA.UnpackEncodedString(DFA9_acceptS);
    static readonly short[] DFA9_special = DFA.UnpackEncodedString(DFA9_specialS);
    static readonly short[][] DFA9_transition = DFA.UnpackEncodedStringArray(DFA9_transitionS);

    protected class DFA9 : DFA
    {
        public DFA9(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = DFA9_eot;
            this.eof = DFA9_eof;
            this.min = DFA9_min;
            this.max = DFA9_max;
            this.accept = DFA9_accept;
            this.special = DFA9_special;
            this.transition = DFA9_transition;

        }

        override public string Description
        {
            get { return "1:1: Tokens : ( IF | ELSE | FOR | WHILE | DO | RETURN | FUNCTION | PROGRAM | TRUE | FALSE | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | WS | ML_COMMENT | NUMBER | IDENT | ADD | SUB | MUL | DIV | MOD | XOR | AND | OR | NOT | BIT_AND | BIT_OR | ASSIGN | UNARY | GE | LE | NEQUALS | EQUALS | GT | LT );"; }
        }

    }

 
    
}
}