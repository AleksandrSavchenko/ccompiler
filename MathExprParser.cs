// $ANTLR 3.2 Sep 23, 2009 12:02:23 MathExpr.g 2015-12-22 23:07:09

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;

using IDictionary	= System.Collections.IDictionary;
using Hashtable 	= System.Collections.Hashtable;

using Antlr.Runtime.Tree;

namespace  MathExpr 
{
public partial class MathExprParser : Parser
{
    public static readonly string[] tokenNames = new string[] 
	{
        "<invalid>", 
		"<EOR>", 
		"<DOWN>", 
		"<UP>", 
		"UNKNOWN", 
		"BLOCK", 
		"PARAMS", 
		"ARGUMENTS", 
		"CALL", 
		"VAR", 
		"VAR_ARRAY", 
		"ARRAY_INDEX", 
		"ARRAY_CALL", 
		"ARRAY_SET", 
		"ARRAY_INIT", 
		"IF", 
		"ELSE", 
		"FOR", 
		"WHILE", 
		"DO", 
		"RETURN", 
		"FUNCTION", 
		"PROGRAM", 
		"TRUE", 
		"FALSE", 
		"WS", 
		"ML_COMMENT", 
		"NUMBER", 
		"IDENT", 
		"ADD", 
		"SUB", 
		"MUL", 
		"DIV", 
		"MOD", 
		"XOR", 
		"AND", 
		"OR", 
		"NOT", 
		"BIT_AND", 
		"BIT_OR", 
		"ASSIGN", 
		"UNARY", 
		"GE", 
		"LE", 
		"NEQUALS", 
		"EQUALS", 
		"GT", 
		"LT", 
		"','", 
		"'('", 
		"')'", 
		"'['", 
		"']'", 
		"'{'", 
		"'}'", 
		"';'"
    };

    public const int FUNCTION = 21;
    public const int LT = 47;
    public const int BIT_AND = 38;
    public const int WHILE = 18;
    public const int MOD = 33;
    public const int FOR = 17;
    public const int DO = 19;
    public const int SUB = 30;
    public const int EQUALS = 45;
    public const int NOT = 37;
    public const int AND = 35;
    public const int EOF = -1;
    public const int NEQUALS = 44;
    public const int IF = 15;
    public const int T__55 = 55;
    public const int ML_COMMENT = 26;
    public const int T__51 = 51;
    public const int T__52 = 52;
    public const int ARRAY_SET = 13;
    public const int T__53 = 53;
    public const int BIT_OR = 39;
    public const int UNKNOWN = 4;
    public const int T__54 = 54;
    public const int ARRAY_INDEX = 11;
    public const int RETURN = 20;
    public const int IDENT = 28;
    public const int ARRAY_CALL = 12;
    public const int VAR = 9;
    public const int UNARY = 41;
    public const int ARRAY_INIT = 14;
    public const int T__50 = 50;
    public const int PARAMS = 6;
    public const int ADD = 29;
    public const int GE = 42;
    public const int XOR = 34;
    public const int ARGUMENTS = 7;
    public const int T__48 = 48;
    public const int T__49 = 49;
    public const int ELSE = 16;
    public const int NUMBER = 27;
    public const int VAR_ARRAY = 10;
    public const int TRUE = 23;
    public const int MUL = 31;
    public const int WS = 25;
    public const int BLOCK = 5;
    public const int OR = 36;
    public const int ASSIGN = 40;
    public const int GT = 46;
    public const int PROGRAM = 22;
    public const int CALL = 8;
    public const int DIV = 32;
    public const int FALSE = 24;
    public const int LE = 43;

    // delegates
    // delegators



        public MathExprParser(ITokenStream input)
    		: this(input, new RecognizerSharedState()) {
        }

        public MathExprParser(ITokenStream input, RecognizerSharedState state)
    		: base(input, state) {
            InitializeCyclicDFAs();
            this.state.ruleMemo = new Hashtable[93+1];
             
             
        }
        
    protected ITreeAdaptor adaptor = new CommonTreeAdaptor();

    public ITreeAdaptor TreeAdaptor
    {
        get { return this.adaptor; }
        set {
    	this.adaptor = value;
    	}
    }

    override public string[] TokenNames {
		get { return MathExprParser.tokenNames; }
    }

    override public string GrammarFileName {
		get { return "MathExpr.g"; }
    }


    public class ident_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "ident"
    // MathExpr.g:94:1: ident : IDENT ;
    public MathExprParser.ident_return ident() // throws RecognitionException [1]
    {   
        MathExprParser.ident_return retval = new MathExprParser.ident_return();
        retval.Start = input.LT(1);
        int ident_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken IDENT1 = null;

        AstNode IDENT1_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 1) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:94:6: ( IDENT )
            // MathExpr.g:94:8: IDENT
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	IDENT1=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_ident853); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{IDENT1_tree = (AstNode)adaptor.Create(IDENT1);
            		adaptor.AddChild(root_0, IDENT1_tree);
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 1, ident_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "ident"

    public class type_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "type"
    // MathExpr.g:96:1: type : IDENT ;
    public MathExprParser.type_return type() // throws RecognitionException [1]
    {   
        MathExprParser.type_return retval = new MathExprParser.type_return();
        retval.Start = input.LT(1);
        int type_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken IDENT2 = null;

        AstNode IDENT2_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 2) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:96:5: ( IDENT )
            // MathExpr.g:96:7: IDENT
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	IDENT2=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_type860); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{IDENT2_tree = (AstNode)adaptor.Create(IDENT2);
            		root_0 = (AstNode)adaptor.BecomeRoot(IDENT2_tree, root_0);
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 2, type_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "type"

    public class params__return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "params_"
    // MathExpr.g:98:1: params_ : ( term ( ',' term )* )? ;
    public MathExprParser.params__return params_() // throws RecognitionException [1]
    {   
        MathExprParser.params__return retval = new MathExprParser.params__return();
        retval.Start = input.LT(1);
        int params__StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal4 = null;
        MathExprParser.term_return term3 = default(MathExprParser.term_return);

        MathExprParser.term_return term5 = default(MathExprParser.term_return);


        AstNode char_literal4_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 3) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:98:8: ( ( term ( ',' term )* )? )
            // MathExpr.g:98:10: ( term ( ',' term )* )?
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	// MathExpr.g:98:10: ( term ( ',' term )* )?
            	int alt2 = 2;
            	int LA2_0 = input.LA(1);

            	if ( ((LA2_0 >= TRUE && LA2_0 <= FALSE) || (LA2_0 >= NUMBER && LA2_0 <= IDENT) || LA2_0 == NOT || LA2_0 == 49) )
            	{
            	    alt2 = 1;
            	}
            	switch (alt2) 
            	{
            	    case 1 :
            	        // MathExpr.g:98:12: term ( ',' term )*
            	        {
            	        	PushFollow(FOLLOW_term_in_params_870);
            	        	term3 = term();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term3.Tree);
            	        	// MathExpr.g:98:17: ( ',' term )*
            	        	do 
            	        	{
            	        	    int alt1 = 2;
            	        	    int LA1_0 = input.LA(1);

            	        	    if ( (LA1_0 == 48) )
            	        	    {
            	        	        alt1 = 1;
            	        	    }


            	        	    switch (alt1) 
            	        		{
            	        			case 1 :
            	        			    // MathExpr.g:98:18: ',' term
            	        			    {
            	        			    	char_literal4=(IToken)Match(input,48,FOLLOW_48_in_params_873); if (state.failed) return retval;
            	        			    	PushFollow(FOLLOW_term_in_params_876);
            	        			    	term5 = term();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term5.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    goto loop1;
            	        	    }
            	        	} while (true);

            	        	loop1:
            	        		;	// Stops C# compiler whining that label 'loop1' has no statements


            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 3, params__StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "params_"

    public class call_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "call"
    // MathExpr.g:99:1: call : ident '(' params_ ')' -> ^( CALL ident ^( PARAMS ( params_ )? ) ) ;
    public MathExprParser.call_return call() // throws RecognitionException [1]
    {   
        MathExprParser.call_return retval = new MathExprParser.call_return();
        retval.Start = input.LT(1);
        int call_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal7 = null;
        IToken char_literal9 = null;
        MathExprParser.ident_return ident6 = default(MathExprParser.ident_return);

        MathExprParser.params__return params_8 = default(MathExprParser.params__return);


        AstNode char_literal7_tree=null;
        AstNode char_literal9_tree=null;
        RewriteRuleTokenStream stream_49 = new RewriteRuleTokenStream(adaptor,"token 49");
        RewriteRuleTokenStream stream_50 = new RewriteRuleTokenStream(adaptor,"token 50");
        RewriteRuleSubtreeStream stream_ident = new RewriteRuleSubtreeStream(adaptor,"rule ident");
        RewriteRuleSubtreeStream stream_params_ = new RewriteRuleSubtreeStream(adaptor,"rule params_");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 4) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:99:5: ( ident '(' params_ ')' -> ^( CALL ident ^( PARAMS ( params_ )? ) ) )
            // MathExpr.g:99:7: ident '(' params_ ')'
            {
            	PushFollow(FOLLOW_ident_in_call889);
            	ident6 = ident();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_ident.Add(ident6.Tree);
            	char_literal7=(IToken)Match(input,49,FOLLOW_49_in_call891); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_49.Add(char_literal7);

            	PushFollow(FOLLOW_params__in_call893);
            	params_8 = params_();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_params_.Add(params_8.Tree);
            	char_literal9=(IToken)Match(input,50,FOLLOW_50_in_call895); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_50.Add(char_literal9);



            	// AST REWRITE
            	// elements:          params_, ident
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (AstNode)adaptor.GetNilNode();
            	// 99:30: -> ^( CALL ident ^( PARAMS ( params_ )? ) )
            	{
            	    // MathExpr.g:99:33: ^( CALL ident ^( PARAMS ( params_ )? ) )
            	    {
            	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
            	    root_1 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(CALL, "CALL"), root_1);

            	    adaptor.AddChild(root_1, stream_ident.NextTree());
            	    // MathExpr.g:99:46: ^( PARAMS ( params_ )? )
            	    {
            	    AstNode root_2 = (AstNode)adaptor.GetNilNode();
            	    root_2 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(PARAMS, "PARAMS"), root_2);

            	    // MathExpr.g:99:55: ( params_ )?
            	    if ( stream_params_.HasNext() )
            	    {
            	        adaptor.AddChild(root_2, stream_params_.NextTree());

            	    }
            	    stream_params_.Reset();

            	    adaptor.AddChild(root_1, root_2);
            	    }

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 4, call_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "call"

    public class callArray_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "callArray"
    // MathExpr.g:102:1: callArray : ident arrayIndex -> ^( ARRAY_CALL ident arrayIndex ) ;
    public MathExprParser.callArray_return callArray() // throws RecognitionException [1]
    {   
        MathExprParser.callArray_return retval = new MathExprParser.callArray_return();
        retval.Start = input.LT(1);
        int callArray_StartIndex = input.Index();
        AstNode root_0 = null;

        MathExprParser.ident_return ident10 = default(MathExprParser.ident_return);

        MathExprParser.arrayIndex_return arrayIndex11 = default(MathExprParser.arrayIndex_return);


        RewriteRuleSubtreeStream stream_arrayIndex = new RewriteRuleSubtreeStream(adaptor,"rule arrayIndex");
        RewriteRuleSubtreeStream stream_ident = new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 5) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:102:10: ( ident arrayIndex -> ^( ARRAY_CALL ident arrayIndex ) )
            // MathExpr.g:102:12: ident arrayIndex
            {
            	PushFollow(FOLLOW_ident_in_callArray920);
            	ident10 = ident();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_ident.Add(ident10.Tree);
            	PushFollow(FOLLOW_arrayIndex_in_callArray922);
            	arrayIndex11 = arrayIndex();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_arrayIndex.Add(arrayIndex11.Tree);


            	// AST REWRITE
            	// elements:          ident, arrayIndex
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (AstNode)adaptor.GetNilNode();
            	// 102:30: -> ^( ARRAY_CALL ident arrayIndex )
            	{
            	    // MathExpr.g:102:33: ^( ARRAY_CALL ident arrayIndex )
            	    {
            	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
            	    root_1 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(ARRAY_CALL, "ARRAY_CALL"), root_1);

            	    adaptor.AddChild(root_1, stream_ident.NextTree());
            	    adaptor.AddChild(root_1, stream_arrayIndex.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 5, callArray_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "callArray"

    public class group_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "group"
    // MathExpr.g:104:1: group : ( '(' term ')' | NUMBER | TRUE | FALSE | ident | call | callArray );
    public MathExprParser.group_return group() // throws RecognitionException [1]
    {   
        MathExprParser.group_return retval = new MathExprParser.group_return();
        retval.Start = input.LT(1);
        int group_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal12 = null;
        IToken char_literal14 = null;
        IToken NUMBER15 = null;
        IToken TRUE16 = null;
        IToken FALSE17 = null;
        MathExprParser.term_return term13 = default(MathExprParser.term_return);

        MathExprParser.ident_return ident18 = default(MathExprParser.ident_return);

        MathExprParser.call_return call19 = default(MathExprParser.call_return);

        MathExprParser.callArray_return callArray20 = default(MathExprParser.callArray_return);


        AstNode char_literal12_tree=null;
        AstNode char_literal14_tree=null;
        AstNode NUMBER15_tree=null;
        AstNode TRUE16_tree=null;
        AstNode FALSE17_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 6) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:104:6: ( '(' term ')' | NUMBER | TRUE | FALSE | ident | call | callArray )
            int alt3 = 7;
            switch ( input.LA(1) ) 
            {
            case 49:
            	{
                alt3 = 1;
                }
                break;
            case NUMBER:
            	{
                alt3 = 2;
                }
                break;
            case TRUE:
            	{
                alt3 = 3;
                }
                break;
            case FALSE:
            	{
                alt3 = 4;
                }
                break;
            case IDENT:
            	{
                switch ( input.LA(2) ) 
                {
                case EOF:
                case ADD:
                case SUB:
                case MUL:
                case DIV:
                case MOD:
                case XOR:
                case AND:
                case OR:
                case BIT_AND:
                case BIT_OR:
                case GE:
                case LE:
                case NEQUALS:
                case EQUALS:
                case GT:
                case LT:
                case 48:
                case 50:
                case 52:
                case 55:
                	{
                    alt3 = 5;
                    }
                    break;
                case 51:
                	{
                    alt3 = 7;
                    }
                    break;
                case 49:
                	{
                    alt3 = 6;
                    }
                    break;
                	default:
                	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                	    NoViableAltException nvae_d3s5 =
                	        new NoViableAltException("", 3, 5, input);

                	    throw nvae_d3s5;
                }

                }
                break;
            	default:
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    NoViableAltException nvae_d3s0 =
            	        new NoViableAltException("", 3, 0, input);

            	    throw nvae_d3s0;
            }

            switch (alt3) 
            {
                case 1 :
                    // MathExpr.g:105:3: '(' term ')'
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	char_literal12=(IToken)Match(input,49,FOLLOW_49_in_group943); if (state.failed) return retval;
                    	PushFollow(FOLLOW_term_in_group946);
                    	term13 = term();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term13.Tree);
                    	char_literal14=(IToken)Match(input,50,FOLLOW_50_in_group948); if (state.failed) return retval;

                    }
                    break;
                case 2 :
                    // MathExpr.g:106:3: NUMBER
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	NUMBER15=(IToken)Match(input,NUMBER,FOLLOW_NUMBER_in_group953); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{NUMBER15_tree = (AstNode)adaptor.Create(NUMBER15);
                    		adaptor.AddChild(root_0, NUMBER15_tree);
                    	}

                    }
                    break;
                case 3 :
                    // MathExpr.g:107:3: TRUE
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	TRUE16=(IToken)Match(input,TRUE,FOLLOW_TRUE_in_group957); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{TRUE16_tree = (AstNode)adaptor.Create(TRUE16);
                    		adaptor.AddChild(root_0, TRUE16_tree);
                    	}

                    }
                    break;
                case 4 :
                    // MathExpr.g:108:3: FALSE
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	FALSE17=(IToken)Match(input,FALSE,FOLLOW_FALSE_in_group961); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{FALSE17_tree = (AstNode)adaptor.Create(FALSE17);
                    		adaptor.AddChild(root_0, FALSE17_tree);
                    	}

                    }
                    break;
                case 5 :
                    // MathExpr.g:109:3: ident
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_ident_in_group965);
                    	ident18 = ident();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, ident18.Tree);

                    }
                    break;
                case 6 :
                    // MathExpr.g:110:3: call
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_call_in_group969);
                    	call19 = call();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, call19.Tree);

                    }
                    break;
                case 7 :
                    // MathExpr.g:111:3: callArray
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_callArray_in_group973);
                    	callArray20 = callArray();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, callArray20.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 6, group_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "group"

    public class not_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "not"
    // MathExpr.g:114:1: not : ( group | NOT not );
    public MathExprParser.not_return not() // throws RecognitionException [1]
    {   
        MathExprParser.not_return retval = new MathExprParser.not_return();
        retval.Start = input.LT(1);
        int not_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken NOT22 = null;
        MathExprParser.group_return group21 = default(MathExprParser.group_return);

        MathExprParser.not_return not23 = default(MathExprParser.not_return);


        AstNode NOT22_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 7) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:114:4: ( group | NOT not )
            int alt4 = 2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0 >= TRUE && LA4_0 <= FALSE) || (LA4_0 >= NUMBER && LA4_0 <= IDENT) || LA4_0 == 49) )
            {
                alt4 = 1;
            }
            else if ( (LA4_0 == NOT) )
            {
                alt4 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d4s0 =
                    new NoViableAltException("", 4, 0, input);

                throw nvae_d4s0;
            }
            switch (alt4) 
            {
                case 1 :
                    // MathExpr.g:114:8: group
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_group_in_not983);
                    	group21 = group();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, group21.Tree);

                    }
                    break;
                case 2 :
                    // MathExpr.g:114:16: NOT not
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	NOT22=(IToken)Match(input,NOT,FOLLOW_NOT_in_not987); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{NOT22_tree = (AstNode)adaptor.Create(NOT22);
                    		root_0 = (AstNode)adaptor.BecomeRoot(NOT22_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_not_in_not990);
                    	not23 = not();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, not23.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 7, not_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "not"

    public class mult_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "mult"
    // MathExpr.g:115:1: mult : not ( ( MUL | DIV | BIT_AND | MOD | XOR ) not )* ;
    public MathExprParser.mult_return mult() // throws RecognitionException [1]
    {   
        MathExprParser.mult_return retval = new MathExprParser.mult_return();
        retval.Start = input.LT(1);
        int mult_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken set25 = null;
        MathExprParser.not_return not24 = default(MathExprParser.not_return);

        MathExprParser.not_return not26 = default(MathExprParser.not_return);


        AstNode set25_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 8) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:115:5: ( not ( ( MUL | DIV | BIT_AND | MOD | XOR ) not )* )
            // MathExpr.g:115:8: not ( ( MUL | DIV | BIT_AND | MOD | XOR ) not )*
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	PushFollow(FOLLOW_not_in_mult998);
            	not24 = not();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, not24.Tree);
            	// MathExpr.g:115:12: ( ( MUL | DIV | BIT_AND | MOD | XOR ) not )*
            	do 
            	{
            	    int alt5 = 2;
            	    int LA5_0 = input.LA(1);

            	    if ( ((LA5_0 >= MUL && LA5_0 <= XOR) || LA5_0 == BIT_AND) )
            	    {
            	        alt5 = 1;
            	    }


            	    switch (alt5) 
            		{
            			case 1 :
            			    // MathExpr.g:115:14: ( MUL | DIV | BIT_AND | MOD | XOR ) not
            			    {
            			    	set25=(IToken)input.LT(1);
            			    	set25 = (IToken)input.LT(1);
            			    	if ( (input.LA(1) >= MUL && input.LA(1) <= XOR) || input.LA(1) == BIT_AND ) 
            			    	{
            			    	    input.Consume();
            			    	    if ( state.backtracking == 0 ) root_0 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(set25), root_0);
            			    	    state.errorRecovery = false;state.failed = false;
            			    	}
            			    	else 
            			    	{
            			    	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    throw mse;
            			    	}

            			    	PushFollow(FOLLOW_not_in_mult1025);
            			    	not26 = not();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, not26.Tree);

            			    }
            			    break;

            			default:
            			    goto loop5;
            	    }
            	} while (true);

            	loop5:
            		;	// Stops C# compiler whining that label 'loop5' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 8, mult_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "mult"

    public class add_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "add"
    // MathExpr.g:116:1: add : mult ( ( ADD | SUB | BIT_OR ) mult )* ;
    public MathExprParser.add_return add() // throws RecognitionException [1]
    {   
        MathExprParser.add_return retval = new MathExprParser.add_return();
        retval.Start = input.LT(1);
        int add_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken set28 = null;
        MathExprParser.mult_return mult27 = default(MathExprParser.mult_return);

        MathExprParser.mult_return mult29 = default(MathExprParser.mult_return);


        AstNode set28_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 9) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:116:4: ( mult ( ( ADD | SUB | BIT_OR ) mult )* )
            // MathExpr.g:116:8: mult ( ( ADD | SUB | BIT_OR ) mult )*
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	PushFollow(FOLLOW_mult_in_add1038);
            	mult27 = mult();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, mult27.Tree);
            	// MathExpr.g:116:14: ( ( ADD | SUB | BIT_OR ) mult )*
            	do 
            	{
            	    int alt6 = 2;
            	    int LA6_0 = input.LA(1);

            	    if ( ((LA6_0 >= ADD && LA6_0 <= SUB) || LA6_0 == BIT_OR) )
            	    {
            	        alt6 = 1;
            	    }


            	    switch (alt6) 
            		{
            			case 1 :
            			    // MathExpr.g:116:16: ( ADD | SUB | BIT_OR ) mult
            			    {
            			    	set28=(IToken)input.LT(1);
            			    	set28 = (IToken)input.LT(1);
            			    	if ( (input.LA(1) >= ADD && input.LA(1) <= SUB) || input.LA(1) == BIT_OR ) 
            			    	{
            			    	    input.Consume();
            			    	    if ( state.backtracking == 0 ) root_0 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(set28), root_0);
            			    	    state.errorRecovery = false;state.failed = false;
            			    	}
            			    	else 
            			    	{
            			    	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    throw mse;
            			    	}

            			    	PushFollow(FOLLOW_mult_in_add1058);
            			    	mult29 = mult();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, mult29.Tree);

            			    }
            			    break;

            			default:
            			    goto loop6;
            	    }
            	} while (true);

            	loop6:
            		;	// Stops C# compiler whining that label 'loop6' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 9, add_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "add"

    public class compare_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "compare"
    // MathExpr.g:117:1: compare : add ( ( GE | LE | NEQUALS | EQUALS | GT | LT ) add )? ;
    public MathExprParser.compare_return compare() // throws RecognitionException [1]
    {   
        MathExprParser.compare_return retval = new MathExprParser.compare_return();
        retval.Start = input.LT(1);
        int compare_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken set31 = null;
        MathExprParser.add_return add30 = default(MathExprParser.add_return);

        MathExprParser.add_return add32 = default(MathExprParser.add_return);


        AstNode set31_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 10) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:117:8: ( add ( ( GE | LE | NEQUALS | EQUALS | GT | LT ) add )? )
            // MathExpr.g:117:10: add ( ( GE | LE | NEQUALS | EQUALS | GT | LT ) add )?
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	PushFollow(FOLLOW_add_in_compare1087);
            	add30 = add();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, add30.Tree);
            	// MathExpr.g:117:14: ( ( GE | LE | NEQUALS | EQUALS | GT | LT ) add )?
            	int alt7 = 2;
            	int LA7_0 = input.LA(1);

            	if ( ((LA7_0 >= GE && LA7_0 <= LT)) )
            	{
            	    alt7 = 1;
            	}
            	switch (alt7) 
            	{
            	    case 1 :
            	        // MathExpr.g:117:16: ( GE | LE | NEQUALS | EQUALS | GT | LT ) add
            	        {
            	        	set31=(IToken)input.LT(1);
            	        	set31 = (IToken)input.LT(1);
            	        	if ( (input.LA(1) >= GE && input.LA(1) <= LT) ) 
            	        	{
            	        	    input.Consume();
            	        	    if ( state.backtracking == 0 ) root_0 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(set31), root_0);
            	        	    state.errorRecovery = false;state.failed = false;
            	        	}
            	        	else 
            	        	{
            	        	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	        	    throw mse;
            	        	}

            	        	PushFollow(FOLLOW_add_in_compare1118);
            	        	add32 = add();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, add32.Tree);

            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 10, compare_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "compare"

    public class and_logic_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "and_logic"
    // MathExpr.g:118:1: and_logic : compare ( AND compare )* ;
    public MathExprParser.and_logic_return and_logic() // throws RecognitionException [1]
    {   
        MathExprParser.and_logic_return retval = new MathExprParser.and_logic_return();
        retval.Start = input.LT(1);
        int and_logic_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken AND34 = null;
        MathExprParser.compare_return compare33 = default(MathExprParser.compare_return);

        MathExprParser.compare_return compare35 = default(MathExprParser.compare_return);


        AstNode AND34_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 11) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:118:10: ( compare ( AND compare )* )
            // MathExpr.g:118:12: compare ( AND compare )*
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	PushFollow(FOLLOW_compare_in_and_logic1130);
            	compare33 = compare();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, compare33.Tree);
            	// MathExpr.g:118:20: ( AND compare )*
            	do 
            	{
            	    int alt8 = 2;
            	    int LA8_0 = input.LA(1);

            	    if ( (LA8_0 == AND) )
            	    {
            	        alt8 = 1;
            	    }


            	    switch (alt8) 
            		{
            			case 1 :
            			    // MathExpr.g:118:22: AND compare
            			    {
            			    	AND34=(IToken)Match(input,AND,FOLLOW_AND_in_and_logic1134); if (state.failed) return retval;
            			    	if ( state.backtracking == 0 )
            			    	{AND34_tree = (AstNode)adaptor.Create(AND34);
            			    		root_0 = (AstNode)adaptor.BecomeRoot(AND34_tree, root_0);
            			    	}
            			    	PushFollow(FOLLOW_compare_in_and_logic1137);
            			    	compare35 = compare();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, compare35.Tree);

            			    }
            			    break;

            			default:
            			    goto loop8;
            	    }
            	} while (true);

            	loop8:
            		;	// Stops C# compiler whining that label 'loop8' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 11, and_logic_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "and_logic"

    public class or_logic_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "or_logic"
    // MathExpr.g:119:1: or_logic : and_logic ( OR and_logic )* ;
    public MathExprParser.or_logic_return or_logic() // throws RecognitionException [1]
    {   
        MathExprParser.or_logic_return retval = new MathExprParser.or_logic_return();
        retval.Start = input.LT(1);
        int or_logic_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken OR37 = null;
        MathExprParser.and_logic_return and_logic36 = default(MathExprParser.and_logic_return);

        MathExprParser.and_logic_return and_logic38 = default(MathExprParser.and_logic_return);


        AstNode OR37_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 12) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:119:9: ( and_logic ( OR and_logic )* )
            // MathExpr.g:119:11: and_logic ( OR and_logic )*
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	PushFollow(FOLLOW_and_logic_in_or_logic1150);
            	and_logic36 = and_logic();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, and_logic36.Tree);
            	// MathExpr.g:119:21: ( OR and_logic )*
            	do 
            	{
            	    int alt9 = 2;
            	    int LA9_0 = input.LA(1);

            	    if ( (LA9_0 == OR) )
            	    {
            	        alt9 = 1;
            	    }


            	    switch (alt9) 
            		{
            			case 1 :
            			    // MathExpr.g:119:23: OR and_logic
            			    {
            			    	OR37=(IToken)Match(input,OR,FOLLOW_OR_in_or_logic1154); if (state.failed) return retval;
            			    	if ( state.backtracking == 0 )
            			    	{OR37_tree = (AstNode)adaptor.Create(OR37);
            			    		root_0 = (AstNode)adaptor.BecomeRoot(OR37_tree, root_0);
            			    	}
            			    	PushFollow(FOLLOW_and_logic_in_or_logic1157);
            			    	and_logic38 = and_logic();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, and_logic38.Tree);

            			    }
            			    break;

            			default:
            			    goto loop9;
            	    }
            	} while (true);

            	loop9:
            		;	// Stops C# compiler whining that label 'loop9' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 12, or_logic_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "or_logic"

    public class term_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "term"
    // MathExpr.g:120:1: term : or_logic ;
    public MathExprParser.term_return term() // throws RecognitionException [1]
    {   
        MathExprParser.term_return retval = new MathExprParser.term_return();
        retval.Start = input.LT(1);
        int term_StartIndex = input.Index();
        AstNode root_0 = null;

        MathExprParser.or_logic_return or_logic39 = default(MathExprParser.or_logic_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 13) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:120:5: ( or_logic )
            // MathExpr.g:120:7: or_logic
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	PushFollow(FOLLOW_or_logic_in_term1168);
            	or_logic39 = or_logic();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, or_logic39.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 13, term_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "term"

    public class arrayConst_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "arrayConst"
    // MathExpr.g:123:1: arrayConst : ( '[' NUMBER ']' | '[' NUMBER ']' arrayConst );
    public MathExprParser.arrayConst_return arrayConst() // throws RecognitionException [1]
    {   
        MathExprParser.arrayConst_return retval = new MathExprParser.arrayConst_return();
        retval.Start = input.LT(1);
        int arrayConst_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal40 = null;
        IToken NUMBER41 = null;
        IToken char_literal42 = null;
        IToken char_literal43 = null;
        IToken NUMBER44 = null;
        IToken char_literal45 = null;
        MathExprParser.arrayConst_return arrayConst46 = default(MathExprParser.arrayConst_return);


        AstNode char_literal40_tree=null;
        AstNode NUMBER41_tree=null;
        AstNode char_literal42_tree=null;
        AstNode char_literal43_tree=null;
        AstNode NUMBER44_tree=null;
        AstNode char_literal45_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 14) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:123:11: ( '[' NUMBER ']' | '[' NUMBER ']' arrayConst )
            int alt10 = 2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0 == 51) )
            {
                int LA10_1 = input.LA(2);

                if ( (LA10_1 == NUMBER) )
                {
                    int LA10_2 = input.LA(3);

                    if ( (LA10_2 == 52) )
                    {
                        int LA10_3 = input.LA(4);

                        if ( (LA10_3 == IDENT) )
                        {
                            alt10 = 1;
                        }
                        else if ( (LA10_3 == 51) )
                        {
                            alt10 = 2;
                        }
                        else 
                        {
                            if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                            NoViableAltException nvae_d10s3 =
                                new NoViableAltException("", 10, 3, input);

                            throw nvae_d10s3;
                        }
                    }
                    else 
                    {
                        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                        NoViableAltException nvae_d10s2 =
                            new NoViableAltException("", 10, 2, input);

                        throw nvae_d10s2;
                    }
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d10s1 =
                        new NoViableAltException("", 10, 1, input);

                    throw nvae_d10s1;
                }
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d10s0 =
                    new NoViableAltException("", 10, 0, input);

                throw nvae_d10s0;
            }
            switch (alt10) 
            {
                case 1 :
                    // MathExpr.g:123:13: '[' NUMBER ']'
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	char_literal40=(IToken)Match(input,51,FOLLOW_51_in_arrayConst1178); if (state.failed) return retval;
                    	NUMBER41=(IToken)Match(input,NUMBER,FOLLOW_NUMBER_in_arrayConst1181); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{NUMBER41_tree = (AstNode)adaptor.Create(NUMBER41);
                    		adaptor.AddChild(root_0, NUMBER41_tree);
                    	}
                    	char_literal42=(IToken)Match(input,52,FOLLOW_52_in_arrayConst1183); if (state.failed) return retval;

                    }
                    break;
                case 2 :
                    // MathExpr.g:123:32: '[' NUMBER ']' arrayConst
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	char_literal43=(IToken)Match(input,51,FOLLOW_51_in_arrayConst1188); if (state.failed) return retval;
                    	NUMBER44=(IToken)Match(input,NUMBER,FOLLOW_NUMBER_in_arrayConst1191); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{NUMBER44_tree = (AstNode)adaptor.Create(NUMBER44);
                    		adaptor.AddChild(root_0, NUMBER44_tree);
                    	}
                    	char_literal45=(IToken)Match(input,52,FOLLOW_52_in_arrayConst1193); if (state.failed) return retval;
                    	PushFollow(FOLLOW_arrayConst_in_arrayConst1196);
                    	arrayConst46 = arrayConst();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, arrayConst46.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 14, arrayConst_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "arrayConst"

    public class arrayNumber_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "arrayNumber"
    // MathExpr.g:125:1: arrayNumber : ( NUMBER | NUMBER ',' arrayNumber );
    public MathExprParser.arrayNumber_return arrayNumber() // throws RecognitionException [1]
    {   
        MathExprParser.arrayNumber_return retval = new MathExprParser.arrayNumber_return();
        retval.Start = input.LT(1);
        int arrayNumber_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken NUMBER47 = null;
        IToken NUMBER48 = null;
        IToken char_literal49 = null;
        MathExprParser.arrayNumber_return arrayNumber50 = default(MathExprParser.arrayNumber_return);


        AstNode NUMBER47_tree=null;
        AstNode NUMBER48_tree=null;
        AstNode char_literal49_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 15) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:125:12: ( NUMBER | NUMBER ',' arrayNumber )
            int alt11 = 2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0 == NUMBER) )
            {
                int LA11_1 = input.LA(2);

                if ( (LA11_1 == 48) )
                {
                    alt11 = 2;
                }
                else if ( (LA11_1 == 54) )
                {
                    alt11 = 1;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d11s1 =
                        new NoViableAltException("", 11, 1, input);

                    throw nvae_d11s1;
                }
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d11s0 =
                    new NoViableAltException("", 11, 0, input);

                throw nvae_d11s0;
            }
            switch (alt11) 
            {
                case 1 :
                    // MathExpr.g:125:14: NUMBER
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	NUMBER47=(IToken)Match(input,NUMBER,FOLLOW_NUMBER_in_arrayNumber1203); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{NUMBER47_tree = (AstNode)adaptor.Create(NUMBER47);
                    		adaptor.AddChild(root_0, NUMBER47_tree);
                    	}

                    }
                    break;
                case 2 :
                    // MathExpr.g:125:23: NUMBER ',' arrayNumber
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	NUMBER48=(IToken)Match(input,NUMBER,FOLLOW_NUMBER_in_arrayNumber1207); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{NUMBER48_tree = (AstNode)adaptor.Create(NUMBER48);
                    		adaptor.AddChild(root_0, NUMBER48_tree);
                    	}
                    	char_literal49=(IToken)Match(input,48,FOLLOW_48_in_arrayNumber1209); if (state.failed) return retval;
                    	PushFollow(FOLLOW_arrayNumber_in_arrayNumber1212);
                    	arrayNumber50 = arrayNumber();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, arrayNumber50.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 15, arrayNumber_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "arrayNumber"

    public class arrayInit_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "arrayInit"
    // MathExpr.g:126:1: arrayInit : '{' arrayNumber '}' ;
    public MathExprParser.arrayInit_return arrayInit() // throws RecognitionException [1]
    {   
        MathExprParser.arrayInit_return retval = new MathExprParser.arrayInit_return();
        retval.Start = input.LT(1);
        int arrayInit_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal51 = null;
        IToken char_literal53 = null;
        MathExprParser.arrayNumber_return arrayNumber52 = default(MathExprParser.arrayNumber_return);


        AstNode char_literal51_tree=null;
        AstNode char_literal53_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 16) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:126:10: ( '{' arrayNumber '}' )
            // MathExpr.g:126:12: '{' arrayNumber '}'
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	char_literal51=(IToken)Match(input,53,FOLLOW_53_in_arrayInit1218); if (state.failed) return retval;
            	PushFollow(FOLLOW_arrayNumber_in_arrayInit1221);
            	arrayNumber52 = arrayNumber();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, arrayNumber52.Tree);
            	char_literal53=(IToken)Match(input,54,FOLLOW_54_in_arrayInit1223); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 16, arrayInit_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "arrayInit"

    public class varDecl_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "varDecl"
    // MathExpr.g:128:1: varDecl : ident ( ASSIGN term )? ;
    public MathExprParser.varDecl_return varDecl() // throws RecognitionException [1]
    {   
        MathExprParser.varDecl_return retval = new MathExprParser.varDecl_return();
        retval.Start = input.LT(1);
        int varDecl_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken ASSIGN55 = null;
        MathExprParser.ident_return ident54 = default(MathExprParser.ident_return);

        MathExprParser.term_return term56 = default(MathExprParser.term_return);


        AstNode ASSIGN55_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 17) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:128:8: ( ident ( ASSIGN term )? )
            // MathExpr.g:128:10: ident ( ASSIGN term )?
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	PushFollow(FOLLOW_ident_in_varDecl1231);
            	ident54 = ident();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, ident54.Tree);
            	// MathExpr.g:128:16: ( ASSIGN term )?
            	int alt12 = 2;
            	int LA12_0 = input.LA(1);

            	if ( (LA12_0 == ASSIGN) )
            	{
            	    alt12 = 1;
            	}
            	switch (alt12) 
            	{
            	    case 1 :
            	        // MathExpr.g:128:17: ASSIGN term
            	        {
            	        	ASSIGN55=(IToken)Match(input,ASSIGN,FOLLOW_ASSIGN_in_varDecl1234); if (state.failed) return retval;
            	        	if ( state.backtracking == 0 )
            	        	{ASSIGN55_tree = (AstNode)adaptor.Create(ASSIGN55);
            	        		root_0 = (AstNode)adaptor.BecomeRoot(ASSIGN55_tree, root_0);
            	        	}
            	        	PushFollow(FOLLOW_term_in_varDecl1237);
            	        	term56 = term();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term56.Tree);

            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 17, varDecl_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "varDecl"

    public class varsDecl_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "varsDecl"
    // MathExpr.g:130:1: varsDecl : ( type varDecl ( ',' varDecl )* -> ^( VAR ^( type ( varDecl )+ ) ) | type arrayConst varDecl ( ',' varDecl )* -> ^( VAR_ARRAY ^( type arrayConst ) ( varDecl )+ ) | type arrayConst ident ASSIGN arrayInit -> ^( ASSIGN ^( VAR_ARRAY ^( type arrayConst ) ident ) ^( ARRAY_INIT ( arrayInit )+ ) ) );
    public MathExprParser.varsDecl_return varsDecl() // throws RecognitionException [1]
    {   
        MathExprParser.varsDecl_return retval = new MathExprParser.varsDecl_return();
        retval.Start = input.LT(1);
        int varsDecl_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal59 = null;
        IToken char_literal64 = null;
        IToken ASSIGN69 = null;
        MathExprParser.type_return type57 = default(MathExprParser.type_return);

        MathExprParser.varDecl_return varDecl58 = default(MathExprParser.varDecl_return);

        MathExprParser.varDecl_return varDecl60 = default(MathExprParser.varDecl_return);

        MathExprParser.type_return type61 = default(MathExprParser.type_return);

        MathExprParser.arrayConst_return arrayConst62 = default(MathExprParser.arrayConst_return);

        MathExprParser.varDecl_return varDecl63 = default(MathExprParser.varDecl_return);

        MathExprParser.varDecl_return varDecl65 = default(MathExprParser.varDecl_return);

        MathExprParser.type_return type66 = default(MathExprParser.type_return);

        MathExprParser.arrayConst_return arrayConst67 = default(MathExprParser.arrayConst_return);

        MathExprParser.ident_return ident68 = default(MathExprParser.ident_return);

        MathExprParser.arrayInit_return arrayInit70 = default(MathExprParser.arrayInit_return);


        AstNode char_literal59_tree=null;
        AstNode char_literal64_tree=null;
        AstNode ASSIGN69_tree=null;
        RewriteRuleTokenStream stream_48 = new RewriteRuleTokenStream(adaptor,"token 48");
        RewriteRuleTokenStream stream_ASSIGN = new RewriteRuleTokenStream(adaptor,"token ASSIGN");
        RewriteRuleSubtreeStream stream_arrayInit = new RewriteRuleSubtreeStream(adaptor,"rule arrayInit");
        RewriteRuleSubtreeStream stream_ident = new RewriteRuleSubtreeStream(adaptor,"rule ident");
        RewriteRuleSubtreeStream stream_arrayConst = new RewriteRuleSubtreeStream(adaptor,"rule arrayConst");
        RewriteRuleSubtreeStream stream_varDecl = new RewriteRuleSubtreeStream(adaptor,"rule varDecl");
        RewriteRuleSubtreeStream stream_type = new RewriteRuleSubtreeStream(adaptor,"rule type");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 18) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:130:9: ( type varDecl ( ',' varDecl )* -> ^( VAR ^( type ( varDecl )+ ) ) | type arrayConst varDecl ( ',' varDecl )* -> ^( VAR_ARRAY ^( type arrayConst ) ( varDecl )+ ) | type arrayConst ident ASSIGN arrayInit -> ^( ASSIGN ^( VAR_ARRAY ^( type arrayConst ) ident ) ^( ARRAY_INIT ( arrayInit )+ ) ) )
            int alt15 = 3;
            int LA15_0 = input.LA(1);

            if ( (LA15_0 == IDENT) )
            {
                int LA15_1 = input.LA(2);

                if ( (synpred30_MathExpr()) )
                {
                    alt15 = 1;
                }
                else if ( (synpred32_MathExpr()) )
                {
                    alt15 = 2;
                }
                else if ( (true) )
                {
                    alt15 = 3;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d15s1 =
                        new NoViableAltException("", 15, 1, input);

                    throw nvae_d15s1;
                }
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d15s0 =
                    new NoViableAltException("", 15, 0, input);

                throw nvae_d15s0;
            }
            switch (alt15) 
            {
                case 1 :
                    // MathExpr.g:130:11: type varDecl ( ',' varDecl )*
                    {
                    	PushFollow(FOLLOW_type_in_varsDecl1246);
                    	type57 = type();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_type.Add(type57.Tree);
                    	PushFollow(FOLLOW_varDecl_in_varsDecl1248);
                    	varDecl58 = varDecl();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_varDecl.Add(varDecl58.Tree);
                    	// MathExpr.g:130:24: ( ',' varDecl )*
                    	do 
                    	{
                    	    int alt13 = 2;
                    	    int LA13_0 = input.LA(1);

                    	    if ( (LA13_0 == 48) )
                    	    {
                    	        int LA13_2 = input.LA(2);

                    	        if ( (LA13_2 == IDENT) )
                    	        {
                    	            int LA13_3 = input.LA(3);

                    	            if ( (synpred29_MathExpr()) )
                    	            {
                    	                alt13 = 1;
                    	            }


                    	        }


                    	    }


                    	    switch (alt13) 
                    		{
                    			case 1 :
                    			    // MathExpr.g:130:26: ',' varDecl
                    			    {
                    			    	char_literal59=(IToken)Match(input,48,FOLLOW_48_in_varsDecl1252); if (state.failed) return retval; 
                    			    	if ( (state.backtracking==0) ) stream_48.Add(char_literal59);

                    			    	PushFollow(FOLLOW_varDecl_in_varsDecl1254);
                    			    	varDecl60 = varDecl();
                    			    	state.followingStackPointer--;
                    			    	if (state.failed) return retval;
                    			    	if ( (state.backtracking==0) ) stream_varDecl.Add(varDecl60.Tree);

                    			    }
                    			    break;

                    			default:
                    			    goto loop13;
                    	    }
                    	} while (true);

                    	loop13:
                    		;	// Stops C# compiler whining that label 'loop13' has no statements



                    	// AST REWRITE
                    	// elements:          type, varDecl
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (AstNode)adaptor.GetNilNode();
                    	// 130:41: -> ^( VAR ^( type ( varDecl )+ ) )
                    	{
                    	    // MathExpr.g:130:44: ^( VAR ^( type ( varDecl )+ ) )
                    	    {
                    	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
                    	    root_1 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(VAR, "VAR"), root_1);

                    	    // MathExpr.g:130:50: ^( type ( varDecl )+ )
                    	    {
                    	    AstNode root_2 = (AstNode)adaptor.GetNilNode();
                    	    root_2 = (AstNode)adaptor.BecomeRoot(stream_type.NextNode(), root_2);

                    	    if ( !(stream_varDecl.HasNext()) ) {
                    	        throw new RewriteEarlyExitException();
                    	    }
                    	    while ( stream_varDecl.HasNext() )
                    	    {
                    	        adaptor.AddChild(root_2, stream_varDecl.NextTree());

                    	    }
                    	    stream_varDecl.Reset();

                    	    adaptor.AddChild(root_1, root_2);
                    	    }

                    	    adaptor.AddChild(root_0, root_1);
                    	    }

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;
                case 2 :
                    // MathExpr.g:131:11: type arrayConst varDecl ( ',' varDecl )*
                    {
                    	PushFollow(FOLLOW_type_in_varsDecl1283);
                    	type61 = type();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_type.Add(type61.Tree);
                    	PushFollow(FOLLOW_arrayConst_in_varsDecl1285);
                    	arrayConst62 = arrayConst();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_arrayConst.Add(arrayConst62.Tree);
                    	PushFollow(FOLLOW_varDecl_in_varsDecl1287);
                    	varDecl63 = varDecl();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_varDecl.Add(varDecl63.Tree);
                    	// MathExpr.g:131:35: ( ',' varDecl )*
                    	do 
                    	{
                    	    int alt14 = 2;
                    	    int LA14_0 = input.LA(1);

                    	    if ( (LA14_0 == 48) )
                    	    {
                    	        int LA14_2 = input.LA(2);

                    	        if ( (LA14_2 == IDENT) )
                    	        {
                    	            int LA14_3 = input.LA(3);

                    	            if ( (synpred31_MathExpr()) )
                    	            {
                    	                alt14 = 1;
                    	            }


                    	        }


                    	    }


                    	    switch (alt14) 
                    		{
                    			case 1 :
                    			    // MathExpr.g:131:37: ',' varDecl
                    			    {
                    			    	char_literal64=(IToken)Match(input,48,FOLLOW_48_in_varsDecl1291); if (state.failed) return retval; 
                    			    	if ( (state.backtracking==0) ) stream_48.Add(char_literal64);

                    			    	PushFollow(FOLLOW_varDecl_in_varsDecl1293);
                    			    	varDecl65 = varDecl();
                    			    	state.followingStackPointer--;
                    			    	if (state.failed) return retval;
                    			    	if ( (state.backtracking==0) ) stream_varDecl.Add(varDecl65.Tree);

                    			    }
                    			    break;

                    			default:
                    			    goto loop14;
                    	    }
                    	} while (true);

                    	loop14:
                    		;	// Stops C# compiler whining that label 'loop14' has no statements



                    	// AST REWRITE
                    	// elements:          varDecl, arrayConst, type
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (AstNode)adaptor.GetNilNode();
                    	// 131:52: -> ^( VAR_ARRAY ^( type arrayConst ) ( varDecl )+ )
                    	{
                    	    // MathExpr.g:131:55: ^( VAR_ARRAY ^( type arrayConst ) ( varDecl )+ )
                    	    {
                    	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
                    	    root_1 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(VAR_ARRAY, "VAR_ARRAY"), root_1);

                    	    // MathExpr.g:131:67: ^( type arrayConst )
                    	    {
                    	    AstNode root_2 = (AstNode)adaptor.GetNilNode();
                    	    root_2 = (AstNode)adaptor.BecomeRoot(stream_type.NextNode(), root_2);

                    	    adaptor.AddChild(root_2, stream_arrayConst.NextTree());

                    	    adaptor.AddChild(root_1, root_2);
                    	    }
                    	    if ( !(stream_varDecl.HasNext()) ) {
                    	        throw new RewriteEarlyExitException();
                    	    }
                    	    while ( stream_varDecl.HasNext() )
                    	    {
                    	        adaptor.AddChild(root_1, stream_varDecl.NextTree());

                    	    }
                    	    stream_varDecl.Reset();

                    	    adaptor.AddChild(root_0, root_1);
                    	    }

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;
                case 3 :
                    // MathExpr.g:132:5: type arrayConst ident ASSIGN arrayInit
                    {
                    	PushFollow(FOLLOW_type_in_varsDecl1317);
                    	type66 = type();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_type.Add(type66.Tree);
                    	PushFollow(FOLLOW_arrayConst_in_varsDecl1319);
                    	arrayConst67 = arrayConst();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_arrayConst.Add(arrayConst67.Tree);
                    	PushFollow(FOLLOW_ident_in_varsDecl1321);
                    	ident68 = ident();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_ident.Add(ident68.Tree);
                    	ASSIGN69=(IToken)Match(input,ASSIGN,FOLLOW_ASSIGN_in_varsDecl1323); if (state.failed) return retval; 
                    	if ( (state.backtracking==0) ) stream_ASSIGN.Add(ASSIGN69);

                    	PushFollow(FOLLOW_arrayInit_in_varsDecl1325);
                    	arrayInit70 = arrayInit();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_arrayInit.Add(arrayInit70.Tree);


                    	// AST REWRITE
                    	// elements:          ident, arrayInit, type, arrayConst, ASSIGN
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (AstNode)adaptor.GetNilNode();
                    	// 132:44: -> ^( ASSIGN ^( VAR_ARRAY ^( type arrayConst ) ident ) ^( ARRAY_INIT ( arrayInit )+ ) )
                    	{
                    	    // MathExpr.g:132:47: ^( ASSIGN ^( VAR_ARRAY ^( type arrayConst ) ident ) ^( ARRAY_INIT ( arrayInit )+ ) )
                    	    {
                    	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
                    	    root_1 = (AstNode)adaptor.BecomeRoot(stream_ASSIGN.NextNode(), root_1);

                    	    // MathExpr.g:132:56: ^( VAR_ARRAY ^( type arrayConst ) ident )
                    	    {
                    	    AstNode root_2 = (AstNode)adaptor.GetNilNode();
                    	    root_2 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(VAR_ARRAY, "VAR_ARRAY"), root_2);

                    	    // MathExpr.g:132:68: ^( type arrayConst )
                    	    {
                    	    AstNode root_3 = (AstNode)adaptor.GetNilNode();
                    	    root_3 = (AstNode)adaptor.BecomeRoot(stream_type.NextNode(), root_3);

                    	    adaptor.AddChild(root_3, stream_arrayConst.NextTree());

                    	    adaptor.AddChild(root_2, root_3);
                    	    }
                    	    adaptor.AddChild(root_2, stream_ident.NextTree());

                    	    adaptor.AddChild(root_1, root_2);
                    	    }
                    	    // MathExpr.g:132:94: ^( ARRAY_INIT ( arrayInit )+ )
                    	    {
                    	    AstNode root_2 = (AstNode)adaptor.GetNilNode();
                    	    root_2 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(ARRAY_INIT, "ARRAY_INIT"), root_2);

                    	    if ( !(stream_arrayInit.HasNext()) ) {
                    	        throw new RewriteEarlyExitException();
                    	    }
                    	    while ( stream_arrayInit.HasNext() )
                    	    {
                    	        adaptor.AddChild(root_2, stream_arrayInit.NextTree());

                    	    }
                    	    stream_arrayInit.Reset();

                    	    adaptor.AddChild(root_1, root_2);
                    	    }

                    	    adaptor.AddChild(root_0, root_1);
                    	    }

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 18, varsDecl_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "varsDecl"

    public class arrayIndex_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "arrayIndex"
    // MathExpr.g:134:1: arrayIndex : ( '[' term ']' )+ ;
    public MathExprParser.arrayIndex_return arrayIndex() // throws RecognitionException [1]
    {   
        MathExprParser.arrayIndex_return retval = new MathExprParser.arrayIndex_return();
        retval.Start = input.LT(1);
        int arrayIndex_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal71 = null;
        IToken char_literal73 = null;
        MathExprParser.term_return term72 = default(MathExprParser.term_return);


        AstNode char_literal71_tree=null;
        AstNode char_literal73_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 19) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:134:11: ( ( '[' term ']' )+ )
            // MathExpr.g:135:3: ( '[' term ']' )+
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	// MathExpr.g:135:3: ( '[' term ']' )+
            	int cnt16 = 0;
            	do 
            	{
            	    int alt16 = 2;
            	    int LA16_0 = input.LA(1);

            	    if ( (LA16_0 == 51) )
            	    {
            	        alt16 = 1;
            	    }


            	    switch (alt16) 
            		{
            			case 1 :
            			    // MathExpr.g:135:4: '[' term ']'
            			    {
            			    	char_literal71=(IToken)Match(input,51,FOLLOW_51_in_arrayIndex1361); if (state.failed) return retval;
            			    	PushFollow(FOLLOW_term_in_arrayIndex1364);
            			    	term72 = term();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term72.Tree);
            			    	char_literal73=(IToken)Match(input,52,FOLLOW_52_in_arrayIndex1366); if (state.failed) return retval;

            			    }
            			    break;

            			default:
            			    if ( cnt16 >= 1 ) goto loop16;
            			    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            		            EarlyExitException eee16 =
            		                new EarlyExitException(16, input);
            		            throw eee16;
            	    }
            	    cnt16++;
            	} while (true);

            	loop16:
            		;	// Stops C# compiler whining that label 'loop16' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 19, arrayIndex_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "arrayIndex"

    public class assign0_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "assign0"
    // MathExpr.g:138:1: assign0 : ( ident arrayIndex -> ^( ARRAY_SET ident ^( ARRAY_INDEX arrayIndex ) ) | ident );
    public MathExprParser.assign0_return assign0() // throws RecognitionException [1]
    {   
        MathExprParser.assign0_return retval = new MathExprParser.assign0_return();
        retval.Start = input.LT(1);
        int assign0_StartIndex = input.Index();
        AstNode root_0 = null;

        MathExprParser.ident_return ident74 = default(MathExprParser.ident_return);

        MathExprParser.arrayIndex_return arrayIndex75 = default(MathExprParser.arrayIndex_return);

        MathExprParser.ident_return ident76 = default(MathExprParser.ident_return);


        RewriteRuleSubtreeStream stream_arrayIndex = new RewriteRuleSubtreeStream(adaptor,"rule arrayIndex");
        RewriteRuleSubtreeStream stream_ident = new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 20) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:138:8: ( ident arrayIndex -> ^( ARRAY_SET ident ^( ARRAY_INDEX arrayIndex ) ) | ident )
            int alt17 = 2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0 == IDENT) )
            {
                int LA17_1 = input.LA(2);

                if ( (LA17_1 == ASSIGN) )
                {
                    alt17 = 2;
                }
                else if ( (LA17_1 == 51) )
                {
                    alt17 = 1;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d17s1 =
                        new NoViableAltException("", 17, 1, input);

                    throw nvae_d17s1;
                }
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d17s0 =
                    new NoViableAltException("", 17, 0, input);

                throw nvae_d17s0;
            }
            switch (alt17) 
            {
                case 1 :
                    // MathExpr.g:139:1: ident arrayIndex
                    {
                    	PushFollow(FOLLOW_ident_in_assign01378);
                    	ident74 = ident();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_ident.Add(ident74.Tree);
                    	PushFollow(FOLLOW_arrayIndex_in_assign01380);
                    	arrayIndex75 = arrayIndex();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_arrayIndex.Add(arrayIndex75.Tree);


                    	// AST REWRITE
                    	// elements:          arrayIndex, ident
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (AstNode)adaptor.GetNilNode();
                    	// 139:18: -> ^( ARRAY_SET ident ^( ARRAY_INDEX arrayIndex ) )
                    	{
                    	    // MathExpr.g:139:21: ^( ARRAY_SET ident ^( ARRAY_INDEX arrayIndex ) )
                    	    {
                    	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
                    	    root_1 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(ARRAY_SET, "ARRAY_SET"), root_1);

                    	    adaptor.AddChild(root_1, stream_ident.NextTree());
                    	    // MathExpr.g:139:39: ^( ARRAY_INDEX arrayIndex )
                    	    {
                    	    AstNode root_2 = (AstNode)adaptor.GetNilNode();
                    	    root_2 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(ARRAY_INDEX, "ARRAY_INDEX"), root_2);

                    	    adaptor.AddChild(root_2, stream_arrayIndex.NextTree());

                    	    adaptor.AddChild(root_1, root_2);
                    	    }

                    	    adaptor.AddChild(root_0, root_1);
                    	    }

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;
                case 2 :
                    // MathExpr.g:140:3: ident
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_ident_in_assign01398);
                    	ident76 = ident();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, ident76.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 20, assign0_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "assign0"

    public class expr0_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "expr0"
    // MathExpr.g:143:1: expr0 : ( assign0 ASSIGN term | ident UNARY | UNARY ident | ident -> ^( CALL ident PARAMS ) | call | varsDecl );
    public MathExprParser.expr0_return expr0() // throws RecognitionException [1]
    {   
        MathExprParser.expr0_return retval = new MathExprParser.expr0_return();
        retval.Start = input.LT(1);
        int expr0_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken ASSIGN78 = null;
        IToken UNARY81 = null;
        IToken UNARY82 = null;
        MathExprParser.assign0_return assign077 = default(MathExprParser.assign0_return);

        MathExprParser.term_return term79 = default(MathExprParser.term_return);

        MathExprParser.ident_return ident80 = default(MathExprParser.ident_return);

        MathExprParser.ident_return ident83 = default(MathExprParser.ident_return);

        MathExprParser.ident_return ident84 = default(MathExprParser.ident_return);

        MathExprParser.call_return call85 = default(MathExprParser.call_return);

        MathExprParser.varsDecl_return varsDecl86 = default(MathExprParser.varsDecl_return);


        AstNode ASSIGN78_tree=null;
        AstNode UNARY81_tree=null;
        AstNode UNARY82_tree=null;
        RewriteRuleSubtreeStream stream_ident = new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 21) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:143:6: ( assign0 ASSIGN term | ident UNARY | UNARY ident | ident -> ^( CALL ident PARAMS ) | call | varsDecl )
            int alt18 = 6;
            alt18 = dfa18.Predict(input);
            switch (alt18) 
            {
                case 1 :
                    // MathExpr.g:144:3: assign0 ASSIGN term
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_assign0_in_expr01408);
                    	assign077 = assign0();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, assign077.Tree);
                    	ASSIGN78=(IToken)Match(input,ASSIGN,FOLLOW_ASSIGN_in_expr01410); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{ASSIGN78_tree = (AstNode)adaptor.Create(ASSIGN78);
                    		root_0 = (AstNode)adaptor.BecomeRoot(ASSIGN78_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_term_in_expr01413);
                    	term79 = term();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term79.Tree);

                    }
                    break;
                case 2 :
                    // MathExpr.g:145:3: ident UNARY
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_ident_in_expr01417);
                    	ident80 = ident();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, ident80.Tree);
                    	UNARY81=(IToken)Match(input,UNARY,FOLLOW_UNARY_in_expr01419); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{UNARY81_tree = (AstNode)adaptor.Create(UNARY81);
                    		root_0 = (AstNode)adaptor.BecomeRoot(UNARY81_tree, root_0);
                    	}

                    }
                    break;
                case 3 :
                    // MathExpr.g:146:3: UNARY ident
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	UNARY82=(IToken)Match(input,UNARY,FOLLOW_UNARY_in_expr01424); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{UNARY82_tree = (AstNode)adaptor.Create(UNARY82);
                    		root_0 = (AstNode)adaptor.BecomeRoot(UNARY82_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_ident_in_expr01427);
                    	ident83 = ident();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, ident83.Tree);

                    }
                    break;
                case 4 :
                    // MathExpr.g:147:3: ident
                    {
                    	PushFollow(FOLLOW_ident_in_expr01431);
                    	ident84 = ident();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_ident.Add(ident84.Tree);


                    	// AST REWRITE
                    	// elements:          ident
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (AstNode)adaptor.GetNilNode();
                    	// 147:9: -> ^( CALL ident PARAMS )
                    	{
                    	    // MathExpr.g:147:12: ^( CALL ident PARAMS )
                    	    {
                    	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
                    	    root_1 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(CALL, "CALL"), root_1);

                    	    adaptor.AddChild(root_1, stream_ident.NextTree());
                    	    adaptor.AddChild(root_1, (AstNode)adaptor.Create(PARAMS, "PARAMS"));

                    	    adaptor.AddChild(root_0, root_1);
                    	    }

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;
                case 5 :
                    // MathExpr.g:148:3: call
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_call_in_expr01445);
                    	call85 = call();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, call85.Tree);

                    }
                    break;
                case 6 :
                    // MathExpr.g:149:3: varsDecl
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_varsDecl_in_expr01449);
                    	varsDecl86 = varsDecl();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, varsDecl86.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 21, expr0_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "expr0"

    public class blockExpr_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "blockExpr"
    // MathExpr.g:151:1: blockExpr : '{' exprList '}' ;
    public MathExprParser.blockExpr_return blockExpr() // throws RecognitionException [1]
    {   
        MathExprParser.blockExpr_return retval = new MathExprParser.blockExpr_return();
        retval.Start = input.LT(1);
        int blockExpr_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal87 = null;
        IToken char_literal89 = null;
        MathExprParser.exprList_return exprList88 = default(MathExprParser.exprList_return);


        AstNode char_literal87_tree=null;
        AstNode char_literal89_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 22) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:151:10: ( '{' exprList '}' )
            // MathExpr.g:151:12: '{' exprList '}'
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	char_literal87=(IToken)Match(input,53,FOLLOW_53_in_blockExpr1456); if (state.failed) return retval;
            	PushFollow(FOLLOW_exprList_in_blockExpr1459);
            	exprList88 = exprList();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, exprList88.Tree);
            	char_literal89=(IToken)Match(input,54,FOLLOW_54_in_blockExpr1461); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 22, blockExpr_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "blockExpr"

    public class termOrTrue_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "termOrTrue"
    // MathExpr.g:153:1: termOrTrue : ( term | () -> TRUE );
    public MathExprParser.termOrTrue_return termOrTrue() // throws RecognitionException [1]
    {   
        MathExprParser.termOrTrue_return retval = new MathExprParser.termOrTrue_return();
        retval.Start = input.LT(1);
        int termOrTrue_StartIndex = input.Index();
        AstNode root_0 = null;

        MathExprParser.term_return term90 = default(MathExprParser.term_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 23) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:153:11: ( term | () -> TRUE )
            int alt19 = 2;
            int LA19_0 = input.LA(1);

            if ( ((LA19_0 >= TRUE && LA19_0 <= FALSE) || (LA19_0 >= NUMBER && LA19_0 <= IDENT) || LA19_0 == NOT || LA19_0 == 49) )
            {
                alt19 = 1;
            }
            else if ( (LA19_0 == 55) )
            {
                alt19 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d19s0 =
                    new NoViableAltException("", 19, 0, input);

                throw nvae_d19s0;
            }
            switch (alt19) 
            {
                case 1 :
                    // MathExpr.g:154:3: term
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_term_in_termOrTrue1473);
                    	term90 = term();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term90.Tree);

                    }
                    break;
                case 2 :
                    // MathExpr.g:155:3: ()
                    {
                    	// MathExpr.g:155:3: ()
                    	// MathExpr.g:155:5: 
                    	{
                    	}



                    	// AST REWRITE
                    	// elements:          
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (AstNode)adaptor.GetNilNode();
                    	// 155:7: -> TRUE
                    	{
                    	    adaptor.AddChild(root_0, (AstNode)adaptor.Create(TRUE, "TRUE"));

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 23, termOrTrue_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "termOrTrue"

    public class expr_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "expr"
    // MathExpr.g:158:1: expr : ( expr0 ';' | IF '(' term ')' expr ( ELSE expr )? | WHILE '(' term ')' expr | FOR '(' exprList2 ';' termOrTrue ';' exprList2 ')' expr | DO expr WHILE '(' term ')' | RETURN term ';' | call ';' | callArray ';' | blockExpr );
    public MathExprParser.expr_return expr() // throws RecognitionException [1]
    {   
        MathExprParser.expr_return retval = new MathExprParser.expr_return();
        retval.Start = input.LT(1);
        int expr_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal92 = null;
        IToken IF93 = null;
        IToken char_literal94 = null;
        IToken char_literal96 = null;
        IToken ELSE98 = null;
        IToken WHILE100 = null;
        IToken char_literal101 = null;
        IToken char_literal103 = null;
        IToken FOR105 = null;
        IToken char_literal106 = null;
        IToken char_literal108 = null;
        IToken char_literal110 = null;
        IToken char_literal112 = null;
        IToken DO114 = null;
        IToken WHILE116 = null;
        IToken char_literal117 = null;
        IToken char_literal119 = null;
        IToken RETURN120 = null;
        IToken char_literal122 = null;
        IToken char_literal124 = null;
        IToken char_literal126 = null;
        MathExprParser.expr0_return expr091 = default(MathExprParser.expr0_return);

        MathExprParser.term_return term95 = default(MathExprParser.term_return);

        MathExprParser.expr_return expr97 = default(MathExprParser.expr_return);

        MathExprParser.expr_return expr99 = default(MathExprParser.expr_return);

        MathExprParser.term_return term102 = default(MathExprParser.term_return);

        MathExprParser.expr_return expr104 = default(MathExprParser.expr_return);

        MathExprParser.exprList2_return exprList2107 = default(MathExprParser.exprList2_return);

        MathExprParser.termOrTrue_return termOrTrue109 = default(MathExprParser.termOrTrue_return);

        MathExprParser.exprList2_return exprList2111 = default(MathExprParser.exprList2_return);

        MathExprParser.expr_return expr113 = default(MathExprParser.expr_return);

        MathExprParser.expr_return expr115 = default(MathExprParser.expr_return);

        MathExprParser.term_return term118 = default(MathExprParser.term_return);

        MathExprParser.term_return term121 = default(MathExprParser.term_return);

        MathExprParser.call_return call123 = default(MathExprParser.call_return);

        MathExprParser.callArray_return callArray125 = default(MathExprParser.callArray_return);

        MathExprParser.blockExpr_return blockExpr127 = default(MathExprParser.blockExpr_return);


        AstNode char_literal92_tree=null;
        AstNode IF93_tree=null;
        AstNode char_literal94_tree=null;
        AstNode char_literal96_tree=null;
        AstNode ELSE98_tree=null;
        AstNode WHILE100_tree=null;
        AstNode char_literal101_tree=null;
        AstNode char_literal103_tree=null;
        AstNode FOR105_tree=null;
        AstNode char_literal106_tree=null;
        AstNode char_literal108_tree=null;
        AstNode char_literal110_tree=null;
        AstNode char_literal112_tree=null;
        AstNode DO114_tree=null;
        AstNode WHILE116_tree=null;
        AstNode char_literal117_tree=null;
        AstNode char_literal119_tree=null;
        AstNode RETURN120_tree=null;
        AstNode char_literal122_tree=null;
        AstNode char_literal124_tree=null;
        AstNode char_literal126_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 24) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:158:5: ( expr0 ';' | IF '(' term ')' expr ( ELSE expr )? | WHILE '(' term ')' expr | FOR '(' exprList2 ';' termOrTrue ';' exprList2 ')' expr | DO expr WHILE '(' term ')' | RETURN term ';' | call ';' | callArray ';' | blockExpr )
            int alt21 = 9;
            alt21 = dfa21.Predict(input);
            switch (alt21) 
            {
                case 1 :
                    // MathExpr.g:159:3: expr0 ';'
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_expr0_in_expr1493);
                    	expr091 = expr0();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expr091.Tree);
                    	char_literal92=(IToken)Match(input,55,FOLLOW_55_in_expr1495); if (state.failed) return retval;

                    }
                    break;
                case 2 :
                    // MathExpr.g:160:3: IF '(' term ')' expr ( ELSE expr )?
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	IF93=(IToken)Match(input,IF,FOLLOW_IF_in_expr1500); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{IF93_tree = (AstNode)adaptor.Create(IF93);
                    		root_0 = (AstNode)adaptor.BecomeRoot(IF93_tree, root_0);
                    	}
                    	char_literal94=(IToken)Match(input,49,FOLLOW_49_in_expr1503); if (state.failed) return retval;
                    	PushFollow(FOLLOW_term_in_expr1506);
                    	term95 = term();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term95.Tree);
                    	char_literal96=(IToken)Match(input,50,FOLLOW_50_in_expr1508); if (state.failed) return retval;
                    	PushFollow(FOLLOW_expr_in_expr1511);
                    	expr97 = expr();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expr97.Tree);
                    	// MathExpr.g:160:27: ( ELSE expr )?
                    	int alt20 = 2;
                    	int LA20_0 = input.LA(1);

                    	if ( (LA20_0 == ELSE) )
                    	{
                    	    int LA20_1 = input.LA(2);

                    	    if ( (synpred42_MathExpr()) )
                    	    {
                    	        alt20 = 1;
                    	    }
                    	}
                    	switch (alt20) 
                    	{
                    	    case 1 :
                    	        // MathExpr.g:160:28: ELSE expr
                    	        {
                    	        	ELSE98=(IToken)Match(input,ELSE,FOLLOW_ELSE_in_expr1514); if (state.failed) return retval;
                    	        	PushFollow(FOLLOW_expr_in_expr1517);
                    	        	expr99 = expr();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expr99.Tree);

                    	        }
                    	        break;

                    	}


                    }
                    break;
                case 3 :
                    // MathExpr.g:161:3: WHILE '(' term ')' expr
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	WHILE100=(IToken)Match(input,WHILE,FOLLOW_WHILE_in_expr1523); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{WHILE100_tree = (AstNode)adaptor.Create(WHILE100);
                    		root_0 = (AstNode)adaptor.BecomeRoot(WHILE100_tree, root_0);
                    	}
                    	char_literal101=(IToken)Match(input,49,FOLLOW_49_in_expr1526); if (state.failed) return retval;
                    	PushFollow(FOLLOW_term_in_expr1529);
                    	term102 = term();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term102.Tree);
                    	char_literal103=(IToken)Match(input,50,FOLLOW_50_in_expr1531); if (state.failed) return retval;
                    	PushFollow(FOLLOW_expr_in_expr1534);
                    	expr104 = expr();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expr104.Tree);

                    }
                    break;
                case 4 :
                    // MathExpr.g:162:3: FOR '(' exprList2 ';' termOrTrue ';' exprList2 ')' expr
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	FOR105=(IToken)Match(input,FOR,FOLLOW_FOR_in_expr1538); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{FOR105_tree = (AstNode)adaptor.Create(FOR105);
                    		root_0 = (AstNode)adaptor.BecomeRoot(FOR105_tree, root_0);
                    	}
                    	char_literal106=(IToken)Match(input,49,FOLLOW_49_in_expr1541); if (state.failed) return retval;
                    	PushFollow(FOLLOW_exprList2_in_expr1544);
                    	exprList2107 = exprList2();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, exprList2107.Tree);
                    	char_literal108=(IToken)Match(input,55,FOLLOW_55_in_expr1546); if (state.failed) return retval;
                    	PushFollow(FOLLOW_termOrTrue_in_expr1549);
                    	termOrTrue109 = termOrTrue();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, termOrTrue109.Tree);
                    	char_literal110=(IToken)Match(input,55,FOLLOW_55_in_expr1551); if (state.failed) return retval;
                    	PushFollow(FOLLOW_exprList2_in_expr1554);
                    	exprList2111 = exprList2();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, exprList2111.Tree);
                    	char_literal112=(IToken)Match(input,50,FOLLOW_50_in_expr1556); if (state.failed) return retval;
                    	PushFollow(FOLLOW_expr_in_expr1559);
                    	expr113 = expr();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expr113.Tree);

                    }
                    break;
                case 5 :
                    // MathExpr.g:163:3: DO expr WHILE '(' term ')'
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	DO114=(IToken)Match(input,DO,FOLLOW_DO_in_expr1563); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{DO114_tree = (AstNode)adaptor.Create(DO114);
                    		root_0 = (AstNode)adaptor.BecomeRoot(DO114_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_expr_in_expr1566);
                    	expr115 = expr();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expr115.Tree);
                    	WHILE116=(IToken)Match(input,WHILE,FOLLOW_WHILE_in_expr1568); if (state.failed) return retval;
                    	char_literal117=(IToken)Match(input,49,FOLLOW_49_in_expr1571); if (state.failed) return retval;
                    	PushFollow(FOLLOW_term_in_expr1574);
                    	term118 = term();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term118.Tree);
                    	char_literal119=(IToken)Match(input,50,FOLLOW_50_in_expr1576); if (state.failed) return retval;

                    }
                    break;
                case 6 :
                    // MathExpr.g:164:3: RETURN term ';'
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	RETURN120=(IToken)Match(input,RETURN,FOLLOW_RETURN_in_expr1581); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{RETURN120_tree = (AstNode)adaptor.Create(RETURN120);
                    		root_0 = (AstNode)adaptor.BecomeRoot(RETURN120_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_term_in_expr1584);
                    	term121 = term();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term121.Tree);
                    	char_literal122=(IToken)Match(input,55,FOLLOW_55_in_expr1586); if (state.failed) return retval;

                    }
                    break;
                case 7 :
                    // MathExpr.g:165:3: call ';'
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_call_in_expr1591);
                    	call123 = call();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, call123.Tree);
                    	char_literal124=(IToken)Match(input,55,FOLLOW_55_in_expr1593); if (state.failed) return retval;

                    }
                    break;
                case 8 :
                    // MathExpr.g:166:3: callArray ';'
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_callArray_in_expr1598);
                    	callArray125 = callArray();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, callArray125.Tree);
                    	char_literal126=(IToken)Match(input,55,FOLLOW_55_in_expr1600); if (state.failed) return retval;

                    }
                    break;
                case 9 :
                    // MathExpr.g:167:3: blockExpr
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_blockExpr_in_expr1605);
                    	blockExpr127 = blockExpr();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, blockExpr127.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 24, expr_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "expr"

    public class exprList_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "exprList"
    // MathExpr.g:170:1: exprList : ( expr ( ( ';' )* expr )* )? ( ';' )* -> ^( BLOCK ( expr )* ) ;
    public MathExprParser.exprList_return exprList() // throws RecognitionException [1]
    {   
        MathExprParser.exprList_return retval = new MathExprParser.exprList_return();
        retval.Start = input.LT(1);
        int exprList_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal129 = null;
        IToken char_literal131 = null;
        MathExprParser.expr_return expr128 = default(MathExprParser.expr_return);

        MathExprParser.expr_return expr130 = default(MathExprParser.expr_return);


        AstNode char_literal129_tree=null;
        AstNode char_literal131_tree=null;
        RewriteRuleTokenStream stream_55 = new RewriteRuleTokenStream(adaptor,"token 55");
        RewriteRuleSubtreeStream stream_expr = new RewriteRuleSubtreeStream(adaptor,"rule expr");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 25) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:170:9: ( ( expr ( ( ';' )* expr )* )? ( ';' )* -> ^( BLOCK ( expr )* ) )
            // MathExpr.g:170:11: ( expr ( ( ';' )* expr )* )? ( ';' )*
            {
            	// MathExpr.g:170:11: ( expr ( ( ';' )* expr )* )?
            	int alt24 = 2;
            	int LA24_0 = input.LA(1);

            	if ( (LA24_0 == IF || (LA24_0 >= FOR && LA24_0 <= RETURN) || LA24_0 == IDENT || LA24_0 == UNARY || LA24_0 == 53) )
            	{
            	    alt24 = 1;
            	}
            	switch (alt24) 
            	{
            	    case 1 :
            	        // MathExpr.g:170:13: expr ( ( ';' )* expr )*
            	        {
            	        	PushFollow(FOLLOW_expr_in_exprList1617);
            	        	expr128 = expr();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_expr.Add(expr128.Tree);
            	        	// MathExpr.g:170:18: ( ( ';' )* expr )*
            	        	do 
            	        	{
            	        	    int alt23 = 2;
            	        	    alt23 = dfa23.Predict(input);
            	        	    switch (alt23) 
            	        		{
            	        			case 1 :
            	        			    // MathExpr.g:170:20: ( ';' )* expr
            	        			    {
            	        			    	// MathExpr.g:170:20: ( ';' )*
            	        			    	do 
            	        			    	{
            	        			    	    int alt22 = 2;
            	        			    	    int LA22_0 = input.LA(1);

            	        			    	    if ( (LA22_0 == 55) )
            	        			    	    {
            	        			    	        alt22 = 1;
            	        			    	    }


            	        			    	    switch (alt22) 
            	        			    		{
            	        			    			case 1 :
            	        			    			    // MathExpr.g:0:0: ';'
            	        			    			    {
            	        			    			    	char_literal129=(IToken)Match(input,55,FOLLOW_55_in_exprList1621); if (state.failed) return retval; 
            	        			    			    	if ( (state.backtracking==0) ) stream_55.Add(char_literal129);


            	        			    			    }
            	        			    			    break;

            	        			    			default:
            	        			    			    goto loop22;
            	        			    	    }
            	        			    	} while (true);

            	        			    	loop22:
            	        			    		;	// Stops C# compiler whining that label 'loop22' has no statements

            	        			    	PushFollow(FOLLOW_expr_in_exprList1624);
            	        			    	expr130 = expr();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( (state.backtracking==0) ) stream_expr.Add(expr130.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    goto loop23;
            	        	    }
            	        	} while (true);

            	        	loop23:
            	        		;	// Stops C# compiler whining that label 'loop23' has no statements


            	        }
            	        break;

            	}

            	// MathExpr.g:170:36: ( ';' )*
            	do 
            	{
            	    int alt25 = 2;
            	    int LA25_0 = input.LA(1);

            	    if ( (LA25_0 == 55) )
            	    {
            	        alt25 = 1;
            	    }


            	    switch (alt25) 
            		{
            			case 1 :
            			    // MathExpr.g:0:0: ';'
            			    {
            			    	char_literal131=(IToken)Match(input,55,FOLLOW_55_in_exprList1632); if (state.failed) return retval; 
            			    	if ( (state.backtracking==0) ) stream_55.Add(char_literal131);


            			    }
            			    break;

            			default:
            			    goto loop25;
            	    }
            	} while (true);

            	loop25:
            		;	// Stops C# compiler whining that label 'loop25' has no statements



            	// AST REWRITE
            	// elements:          expr
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (AstNode)adaptor.GetNilNode();
            	// 170:42: -> ^( BLOCK ( expr )* )
            	{
            	    // MathExpr.g:170:46: ^( BLOCK ( expr )* )
            	    {
            	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
            	    root_1 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(BLOCK, "BLOCK"), root_1);

            	    // MathExpr.g:170:54: ( expr )*
            	    while ( stream_expr.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_expr.NextTree());

            	    }
            	    stream_expr.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 25, exprList_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "exprList"

    public class exprList2_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "exprList2"
    // MathExpr.g:172:1: exprList2 : ( expr0 ( ',' expr0 )* )? -> ^( BLOCK ( expr0 )* ) ;
    public MathExprParser.exprList2_return exprList2() // throws RecognitionException [1]
    {   
        MathExprParser.exprList2_return retval = new MathExprParser.exprList2_return();
        retval.Start = input.LT(1);
        int exprList2_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal133 = null;
        MathExprParser.expr0_return expr0132 = default(MathExprParser.expr0_return);

        MathExprParser.expr0_return expr0134 = default(MathExprParser.expr0_return);


        AstNode char_literal133_tree=null;
        RewriteRuleTokenStream stream_48 = new RewriteRuleTokenStream(adaptor,"token 48");
        RewriteRuleSubtreeStream stream_expr0 = new RewriteRuleSubtreeStream(adaptor,"rule expr0");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 26) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:172:10: ( ( expr0 ( ',' expr0 )* )? -> ^( BLOCK ( expr0 )* ) )
            // MathExpr.g:172:12: ( expr0 ( ',' expr0 )* )?
            {
            	// MathExpr.g:172:12: ( expr0 ( ',' expr0 )* )?
            	int alt27 = 2;
            	int LA27_0 = input.LA(1);

            	if ( (LA27_0 == IDENT || LA27_0 == UNARY) )
            	{
            	    alt27 = 1;
            	}
            	switch (alt27) 
            	{
            	    case 1 :
            	        // MathExpr.g:172:14: expr0 ( ',' expr0 )*
            	        {
            	        	PushFollow(FOLLOW_expr0_in_exprList21655);
            	        	expr0132 = expr0();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_expr0.Add(expr0132.Tree);
            	        	// MathExpr.g:172:20: ( ',' expr0 )*
            	        	do 
            	        	{
            	        	    int alt26 = 2;
            	        	    int LA26_0 = input.LA(1);

            	        	    if ( (LA26_0 == 48) )
            	        	    {
            	        	        alt26 = 1;
            	        	    }


            	        	    switch (alt26) 
            	        		{
            	        			case 1 :
            	        			    // MathExpr.g:172:22: ',' expr0
            	        			    {
            	        			    	char_literal133=(IToken)Match(input,48,FOLLOW_48_in_exprList21659); if (state.failed) return retval; 
            	        			    	if ( (state.backtracking==0) ) stream_48.Add(char_literal133);

            	        			    	PushFollow(FOLLOW_expr0_in_exprList21661);
            	        			    	expr0134 = expr0();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( (state.backtracking==0) ) stream_expr0.Add(expr0134.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    goto loop26;
            	        	    }
            	        	} while (true);

            	        	loop26:
            	        		;	// Stops C# compiler whining that label 'loop26' has no statements


            	        }
            	        break;

            	}



            	// AST REWRITE
            	// elements:          expr0
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (AstNode)adaptor.GetNilNode();
            	// 172:39: -> ^( BLOCK ( expr0 )* )
            	{
            	    // MathExpr.g:172:43: ^( BLOCK ( expr0 )* )
            	    {
            	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
            	    root_1 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(BLOCK, "BLOCK"), root_1);

            	    // MathExpr.g:172:51: ( expr0 )*
            	    while ( stream_expr0.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_expr0.NextTree());

            	    }
            	    stream_expr0.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 26, exprList2_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "exprList2"

    public class paramDecl_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "paramDecl"
    // MathExpr.g:174:1: paramDecl : type IDENT ;
    public MathExprParser.paramDecl_return paramDecl() // throws RecognitionException [1]
    {   
        MathExprParser.paramDecl_return retval = new MathExprParser.paramDecl_return();
        retval.Start = input.LT(1);
        int paramDecl_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken IDENT136 = null;
        MathExprParser.type_return type135 = default(MathExprParser.type_return);


        AstNode IDENT136_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 27) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:174:10: ( type IDENT )
            // MathExpr.g:174:12: type IDENT
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	PushFollow(FOLLOW_type_in_paramDecl1687);
            	type135 = type();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, type135.Tree);
            	IDENT136=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_paramDecl1689); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{IDENT136_tree = (AstNode)adaptor.Create(IDENT136);
            		root_0 = (AstNode)adaptor.BecomeRoot(IDENT136_tree, root_0);
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 27, paramDecl_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "paramDecl"

    public class paramsDecl_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "paramsDecl"
    // MathExpr.g:176:1: paramsDecl : paramDecl ( ',' paramDecl )* ;
    public MathExprParser.paramsDecl_return paramsDecl() // throws RecognitionException [1]
    {   
        MathExprParser.paramsDecl_return retval = new MathExprParser.paramsDecl_return();
        retval.Start = input.LT(1);
        int paramsDecl_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal138 = null;
        MathExprParser.paramDecl_return paramDecl137 = default(MathExprParser.paramDecl_return);

        MathExprParser.paramDecl_return paramDecl139 = default(MathExprParser.paramDecl_return);


        AstNode char_literal138_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 28) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:176:11: ( paramDecl ( ',' paramDecl )* )
            // MathExpr.g:176:13: paramDecl ( ',' paramDecl )*
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	PushFollow(FOLLOW_paramDecl_in_paramsDecl1698);
            	paramDecl137 = paramDecl();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, paramDecl137.Tree);
            	// MathExpr.g:176:23: ( ',' paramDecl )*
            	do 
            	{
            	    int alt28 = 2;
            	    int LA28_0 = input.LA(1);

            	    if ( (LA28_0 == 48) )
            	    {
            	        alt28 = 1;
            	    }


            	    switch (alt28) 
            		{
            			case 1 :
            			    // MathExpr.g:176:25: ',' paramDecl
            			    {
            			    	char_literal138=(IToken)Match(input,48,FOLLOW_48_in_paramsDecl1702); if (state.failed) return retval;
            			    	PushFollow(FOLLOW_paramDecl_in_paramsDecl1705);
            			    	paramDecl139 = paramDecl();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, paramDecl139.Tree);

            			    }
            			    break;

            			default:
            			    goto loop28;
            	    }
            	} while (true);

            	loop28:
            		;	// Stops C# compiler whining that label 'loop28' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 28, paramsDecl_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "paramsDecl"

    public class funcDecl_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "funcDecl"
    // MathExpr.g:178:1: funcDecl : t= type n= ident '(' ( paramsDecl )? ')' '{' exprList '}' -> ^( FUNCTION $t $n ^( ARGUMENTS ( paramsDecl )* ) exprList ) ;
    public MathExprParser.funcDecl_return funcDecl() // throws RecognitionException [1]
    {   
        MathExprParser.funcDecl_return retval = new MathExprParser.funcDecl_return();
        retval.Start = input.LT(1);
        int funcDecl_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal140 = null;
        IToken char_literal142 = null;
        IToken char_literal143 = null;
        IToken char_literal145 = null;
        MathExprParser.type_return t = default(MathExprParser.type_return);

        MathExprParser.ident_return n = default(MathExprParser.ident_return);

        MathExprParser.paramsDecl_return paramsDecl141 = default(MathExprParser.paramsDecl_return);

        MathExprParser.exprList_return exprList144 = default(MathExprParser.exprList_return);


        AstNode char_literal140_tree=null;
        AstNode char_literal142_tree=null;
        AstNode char_literal143_tree=null;
        AstNode char_literal145_tree=null;
        RewriteRuleTokenStream stream_49 = new RewriteRuleTokenStream(adaptor,"token 49");
        RewriteRuleTokenStream stream_53 = new RewriteRuleTokenStream(adaptor,"token 53");
        RewriteRuleTokenStream stream_54 = new RewriteRuleTokenStream(adaptor,"token 54");
        RewriteRuleTokenStream stream_50 = new RewriteRuleTokenStream(adaptor,"token 50");
        RewriteRuleSubtreeStream stream_paramsDecl = new RewriteRuleSubtreeStream(adaptor,"rule paramsDecl");
        RewriteRuleSubtreeStream stream_ident = new RewriteRuleSubtreeStream(adaptor,"rule ident");
        RewriteRuleSubtreeStream stream_type = new RewriteRuleSubtreeStream(adaptor,"rule type");
        RewriteRuleSubtreeStream stream_exprList = new RewriteRuleSubtreeStream(adaptor,"rule exprList");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 29) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:178:9: (t= type n= ident '(' ( paramsDecl )? ')' '{' exprList '}' -> ^( FUNCTION $t $n ^( ARGUMENTS ( paramsDecl )* ) exprList ) )
            // MathExpr.g:179:3: t= type n= ident '(' ( paramsDecl )? ')' '{' exprList '}'
            {
            	PushFollow(FOLLOW_type_in_funcDecl1720);
            	t = type();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_type.Add(t.Tree);
            	PushFollow(FOLLOW_ident_in_funcDecl1724);
            	n = ident();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_ident.Add(n.Tree);
            	char_literal140=(IToken)Match(input,49,FOLLOW_49_in_funcDecl1726); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_49.Add(char_literal140);

            	// MathExpr.g:179:22: ( paramsDecl )?
            	int alt29 = 2;
            	int LA29_0 = input.LA(1);

            	if ( (LA29_0 == IDENT) )
            	{
            	    alt29 = 1;
            	}
            	switch (alt29) 
            	{
            	    case 1 :
            	        // MathExpr.g:0:0: paramsDecl
            	        {
            	        	PushFollow(FOLLOW_paramsDecl_in_funcDecl1728);
            	        	paramsDecl141 = paramsDecl();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_paramsDecl.Add(paramsDecl141.Tree);

            	        }
            	        break;

            	}

            	char_literal142=(IToken)Match(input,50,FOLLOW_50_in_funcDecl1731); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_50.Add(char_literal142);

            	char_literal143=(IToken)Match(input,53,FOLLOW_53_in_funcDecl1735); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_53.Add(char_literal143);

            	PushFollow(FOLLOW_exprList_in_funcDecl1737);
            	exprList144 = exprList();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_exprList.Add(exprList144.Tree);
            	char_literal145=(IToken)Match(input,54,FOLLOW_54_in_funcDecl1739); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_54.Add(char_literal145);



            	// AST REWRITE
            	// elements:          paramsDecl, n, t, exprList
            	// token labels:      
            	// rule labels:       retval, t, n
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);
            	RewriteRuleSubtreeStream stream_t = new RewriteRuleSubtreeStream(adaptor, "rule t", t!=null ? t.Tree : null);
            	RewriteRuleSubtreeStream stream_n = new RewriteRuleSubtreeStream(adaptor, "rule n", n!=null ? n.Tree : null);

            	root_0 = (AstNode)adaptor.GetNilNode();
            	// 181:3: -> ^( FUNCTION $t $n ^( ARGUMENTS ( paramsDecl )* ) exprList )
            	{
            	    // MathExpr.g:181:6: ^( FUNCTION $t $n ^( ARGUMENTS ( paramsDecl )* ) exprList )
            	    {
            	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
            	    root_1 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(FUNCTION, "FUNCTION"), root_1);

            	    adaptor.AddChild(root_1, stream_t.NextTree());
            	    adaptor.AddChild(root_1, stream_n.NextTree());
            	    // MathExpr.g:181:23: ^( ARGUMENTS ( paramsDecl )* )
            	    {
            	    AstNode root_2 = (AstNode)adaptor.GetNilNode();
            	    root_2 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(ARGUMENTS, "ARGUMENTS"), root_2);

            	    // MathExpr.g:181:35: ( paramsDecl )*
            	    while ( stream_paramsDecl.HasNext() )
            	    {
            	        adaptor.AddChild(root_2, stream_paramsDecl.NextTree());

            	    }
            	    stream_paramsDecl.Reset();

            	    adaptor.AddChild(root_1, root_2);
            	    }
            	    adaptor.AddChild(root_1, stream_exprList.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 29, funcDecl_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "funcDecl"

    public class exprOrFuncDecl_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "exprOrFuncDecl"
    // MathExpr.g:184:1: exprOrFuncDecl : ( funcDecl | expr );
    public MathExprParser.exprOrFuncDecl_return exprOrFuncDecl() // throws RecognitionException [1]
    {   
        MathExprParser.exprOrFuncDecl_return retval = new MathExprParser.exprOrFuncDecl_return();
        retval.Start = input.LT(1);
        int exprOrFuncDecl_StartIndex = input.Index();
        AstNode root_0 = null;

        MathExprParser.funcDecl_return funcDecl146 = default(MathExprParser.funcDecl_return);

        MathExprParser.expr_return expr147 = default(MathExprParser.expr_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 30) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:184:15: ( funcDecl | expr )
            int alt30 = 2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0 == IDENT) )
            {
                int LA30_1 = input.LA(2);

                if ( ((LA30_1 >= ASSIGN && LA30_1 <= UNARY) || LA30_1 == 49 || LA30_1 == 51 || LA30_1 == 55) )
                {
                    alt30 = 2;
                }
                else if ( (LA30_1 == IDENT) )
                {
                    int LA30_3 = input.LA(3);

                    if ( (LA30_3 == ASSIGN || LA30_3 == 48 || LA30_3 == 55) )
                    {
                        alt30 = 2;
                    }
                    else if ( (LA30_3 == 49) )
                    {
                        alt30 = 1;
                    }
                    else 
                    {
                        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                        NoViableAltException nvae_d30s3 =
                            new NoViableAltException("", 30, 3, input);

                        throw nvae_d30s3;
                    }
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d30s1 =
                        new NoViableAltException("", 30, 1, input);

                    throw nvae_d30s1;
                }
            }
            else if ( (LA30_0 == IF || (LA30_0 >= FOR && LA30_0 <= RETURN) || LA30_0 == UNARY || LA30_0 == 53) )
            {
                alt30 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d30s0 =
                    new NoViableAltException("", 30, 0, input);

                throw nvae_d30s0;
            }
            switch (alt30) 
            {
                case 1 :
                    // MathExpr.g:184:17: funcDecl
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_funcDecl_in_exprOrFuncDecl1770);
                    	funcDecl146 = funcDecl();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, funcDecl146.Tree);

                    }
                    break;
                case 2 :
                    // MathExpr.g:184:28: expr
                    {
                    	root_0 = (AstNode)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_expr_in_exprOrFuncDecl1774);
                    	expr147 = expr();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expr147.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 30, exprOrFuncDecl_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "exprOrFuncDecl"

    public class program_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "program"
    // MathExpr.g:186:1: program : ( exprOrFuncDecl ( ';' )* )* ;
    public MathExprParser.program_return program() // throws RecognitionException [1]
    {   
        MathExprParser.program_return retval = new MathExprParser.program_return();
        retval.Start = input.LT(1);
        int program_StartIndex = input.Index();
        AstNode root_0 = null;

        IToken char_literal149 = null;
        MathExprParser.exprOrFuncDecl_return exprOrFuncDecl148 = default(MathExprParser.exprOrFuncDecl_return);


        AstNode char_literal149_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 31) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:186:8: ( ( exprOrFuncDecl ( ';' )* )* )
            // MathExpr.g:186:10: ( exprOrFuncDecl ( ';' )* )*
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	// MathExpr.g:186:10: ( exprOrFuncDecl ( ';' )* )*
            	do 
            	{
            	    int alt32 = 2;
            	    int LA32_0 = input.LA(1);

            	    if ( (LA32_0 == IF || (LA32_0 >= FOR && LA32_0 <= RETURN) || LA32_0 == IDENT || LA32_0 == UNARY || LA32_0 == 53) )
            	    {
            	        alt32 = 1;
            	    }


            	    switch (alt32) 
            		{
            			case 1 :
            			    // MathExpr.g:186:12: exprOrFuncDecl ( ';' )*
            			    {
            			    	PushFollow(FOLLOW_exprOrFuncDecl_in_program1784);
            			    	exprOrFuncDecl148 = exprOrFuncDecl();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, exprOrFuncDecl148.Tree);
            			    	// MathExpr.g:186:30: ( ';' )*
            			    	do 
            			    	{
            			    	    int alt31 = 2;
            			    	    int LA31_0 = input.LA(1);

            			    	    if ( (LA31_0 == 55) )
            			    	    {
            			    	        alt31 = 1;
            			    	    }


            			    	    switch (alt31) 
            			    		{
            			    			case 1 :
            			    			    // MathExpr.g:0:0: ';'
            			    			    {
            			    			    	char_literal149=(IToken)Match(input,55,FOLLOW_55_in_program1786); if (state.failed) return retval;

            			    			    }
            			    			    break;

            			    			default:
            			    			    goto loop31;
            			    	    }
            			    	} while (true);

            			    	loop31:
            			    		;	// Stops C# compiler whining that label 'loop31' has no statements


            			    }
            			    break;

            			default:
            			    goto loop32;
            	    }
            	} while (true);

            	loop32:
            		;	// Stops C# compiler whining that label 'loop32' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 31, program_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "program"

    public class result_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "result"
    // MathExpr.g:188:1: result : program -> ^( PROGRAM program ) ;
    public MathExprParser.result_return result() // throws RecognitionException [1]
    {   
        MathExprParser.result_return retval = new MathExprParser.result_return();
        retval.Start = input.LT(1);
        int result_StartIndex = input.Index();
        AstNode root_0 = null;

        MathExprParser.program_return program150 = default(MathExprParser.program_return);


        RewriteRuleSubtreeStream stream_program = new RewriteRuleSubtreeStream(adaptor,"rule program");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 32) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:188:7: ( program -> ^( PROGRAM program ) )
            // MathExpr.g:188:9: program
            {
            	PushFollow(FOLLOW_program_in_result1799);
            	program150 = program();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_program.Add(program150.Tree);


            	// AST REWRITE
            	// elements:          program
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (AstNode)adaptor.GetNilNode();
            	// 188:17: -> ^( PROGRAM program )
            	{
            	    // MathExpr.g:188:20: ^( PROGRAM program )
            	    {
            	    AstNode root_1 = (AstNode)adaptor.GetNilNode();
            	    root_1 = (AstNode)adaptor.BecomeRoot((AstNode)adaptor.Create(PROGRAM, "PROGRAM"), root_1);

            	    adaptor.AddChild(root_1, stream_program.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 32, result_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "result"

    public class execute_return : ParserRuleReturnScope
    {
        private AstNode tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (AstNode) value; }
        }
    };

    // $ANTLR start "execute"
    // MathExpr.g:190:1: execute : result ;
    public MathExprParser.execute_return execute() // throws RecognitionException [1]
    {   
        MathExprParser.execute_return retval = new MathExprParser.execute_return();
        retval.Start = input.LT(1);
        int execute_StartIndex = input.Index();
        AstNode root_0 = null;

        MathExprParser.result_return result151 = default(MathExprParser.result_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 33) ) 
    	    {
    	    	return retval; 
    	    }
            // MathExpr.g:190:8: ( result )
            // MathExpr.g:191:3: result
            {
            	root_0 = (AstNode)adaptor.GetNilNode();

            	PushFollow(FOLLOW_result_in_execute1817);
            	result151 = result();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, result151.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (AstNode)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (AstNode)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 33, execute_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "execute"

    // $ANTLR start "synpred29_MathExpr"
    public void synpred29_MathExpr_fragment() {
        // MathExpr.g:130:26: ( ',' varDecl )
        // MathExpr.g:130:26: ',' varDecl
        {
        	Match(input,48,FOLLOW_48_in_synpred29_MathExpr1252); if (state.failed) return ;
        	PushFollow(FOLLOW_varDecl_in_synpred29_MathExpr1254);
        	varDecl();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred29_MathExpr"

    // $ANTLR start "synpred30_MathExpr"
    public void synpred30_MathExpr_fragment() {
        // MathExpr.g:130:11: ( type varDecl ( ',' varDecl )* )
        // MathExpr.g:130:11: type varDecl ( ',' varDecl )*
        {
        	PushFollow(FOLLOW_type_in_synpred30_MathExpr1246);
        	type();
        	state.followingStackPointer--;
        	if (state.failed) return ;
        	PushFollow(FOLLOW_varDecl_in_synpred30_MathExpr1248);
        	varDecl();
        	state.followingStackPointer--;
        	if (state.failed) return ;
        	// MathExpr.g:130:24: ( ',' varDecl )*
        	do 
        	{
        	    int alt34 = 2;
        	    int LA34_0 = input.LA(1);

        	    if ( (LA34_0 == 48) )
        	    {
        	        alt34 = 1;
        	    }


        	    switch (alt34) 
        		{
        			case 1 :
        			    // MathExpr.g:130:26: ',' varDecl
        			    {
        			    	Match(input,48,FOLLOW_48_in_synpred30_MathExpr1252); if (state.failed) return ;
        			    	PushFollow(FOLLOW_varDecl_in_synpred30_MathExpr1254);
        			    	varDecl();
        			    	state.followingStackPointer--;
        			    	if (state.failed) return ;

        			    }
        			    break;

        			default:
        			    goto loop34;
        	    }
        	} while (true);

        	loop34:
        		;	// Stops C# compiler whining that label 'loop34' has no statements


        }
    }
    // $ANTLR end "synpred30_MathExpr"

    // $ANTLR start "synpred31_MathExpr"
    public void synpred31_MathExpr_fragment() {
        // MathExpr.g:131:37: ( ',' varDecl )
        // MathExpr.g:131:37: ',' varDecl
        {
        	Match(input,48,FOLLOW_48_in_synpred31_MathExpr1291); if (state.failed) return ;
        	PushFollow(FOLLOW_varDecl_in_synpred31_MathExpr1293);
        	varDecl();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred31_MathExpr"

    // $ANTLR start "synpred32_MathExpr"
    public void synpred32_MathExpr_fragment() {
        // MathExpr.g:131:11: ( type arrayConst varDecl ( ',' varDecl )* )
        // MathExpr.g:131:11: type arrayConst varDecl ( ',' varDecl )*
        {
        	PushFollow(FOLLOW_type_in_synpred32_MathExpr1283);
        	type();
        	state.followingStackPointer--;
        	if (state.failed) return ;
        	PushFollow(FOLLOW_arrayConst_in_synpred32_MathExpr1285);
        	arrayConst();
        	state.followingStackPointer--;
        	if (state.failed) return ;
        	PushFollow(FOLLOW_varDecl_in_synpred32_MathExpr1287);
        	varDecl();
        	state.followingStackPointer--;
        	if (state.failed) return ;
        	// MathExpr.g:131:35: ( ',' varDecl )*
        	do 
        	{
        	    int alt35 = 2;
        	    int LA35_0 = input.LA(1);

        	    if ( (LA35_0 == 48) )
        	    {
        	        alt35 = 1;
        	    }


        	    switch (alt35) 
        		{
        			case 1 :
        			    // MathExpr.g:131:37: ',' varDecl
        			    {
        			    	Match(input,48,FOLLOW_48_in_synpred32_MathExpr1291); if (state.failed) return ;
        			    	PushFollow(FOLLOW_varDecl_in_synpred32_MathExpr1293);
        			    	varDecl();
        			    	state.followingStackPointer--;
        			    	if (state.failed) return ;

        			    }
        			    break;

        			default:
        			    goto loop35;
        	    }
        	} while (true);

        	loop35:
        		;	// Stops C# compiler whining that label 'loop35' has no statements


        }
    }
    // $ANTLR end "synpred32_MathExpr"

    // $ANTLR start "synpred35_MathExpr"
    public void synpred35_MathExpr_fragment() {
        // MathExpr.g:144:3: ( assign0 ASSIGN term )
        // MathExpr.g:144:3: assign0 ASSIGN term
        {
        	PushFollow(FOLLOW_assign0_in_synpred35_MathExpr1408);
        	assign0();
        	state.followingStackPointer--;
        	if (state.failed) return ;
        	Match(input,ASSIGN,FOLLOW_ASSIGN_in_synpred35_MathExpr1410); if (state.failed) return ;
        	PushFollow(FOLLOW_term_in_synpred35_MathExpr1413);
        	term();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred35_MathExpr"

    // $ANTLR start "synpred41_MathExpr"
    public void synpred41_MathExpr_fragment() {
        // MathExpr.g:159:3: ( expr0 ';' )
        // MathExpr.g:159:3: expr0 ';'
        {
        	PushFollow(FOLLOW_expr0_in_synpred41_MathExpr1493);
        	expr0();
        	state.followingStackPointer--;
        	if (state.failed) return ;
        	Match(input,55,FOLLOW_55_in_synpred41_MathExpr1495); if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred41_MathExpr"

    // $ANTLR start "synpred42_MathExpr"
    public void synpred42_MathExpr_fragment() {
        // MathExpr.g:160:28: ( ELSE expr )
        // MathExpr.g:160:28: ELSE expr
        {
        	Match(input,ELSE,FOLLOW_ELSE_in_synpred42_MathExpr1514); if (state.failed) return ;
        	PushFollow(FOLLOW_expr_in_synpred42_MathExpr1517);
        	expr();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred42_MathExpr"

    // $ANTLR start "synpred48_MathExpr"
    public void synpred48_MathExpr_fragment() {
        // MathExpr.g:165:3: ( call ';' )
        // MathExpr.g:165:3: call ';'
        {
        	PushFollow(FOLLOW_call_in_synpred48_MathExpr1591);
        	call();
        	state.followingStackPointer--;
        	if (state.failed) return ;
        	Match(input,55,FOLLOW_55_in_synpred48_MathExpr1593); if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred48_MathExpr"

    // $ANTLR start "synpred49_MathExpr"
    public void synpred49_MathExpr_fragment() {
        // MathExpr.g:166:3: ( callArray ';' )
        // MathExpr.g:166:3: callArray ';'
        {
        	PushFollow(FOLLOW_callArray_in_synpred49_MathExpr1598);
        	callArray();
        	state.followingStackPointer--;
        	if (state.failed) return ;
        	Match(input,55,FOLLOW_55_in_synpred49_MathExpr1600); if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred49_MathExpr"

    // Delegated rules

   	public bool synpred30_MathExpr() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred30_MathExpr_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred29_MathExpr() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred29_MathExpr_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred49_MathExpr() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred49_MathExpr_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred48_MathExpr() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred48_MathExpr_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred41_MathExpr() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred41_MathExpr_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred32_MathExpr() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred32_MathExpr_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred42_MathExpr() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred42_MathExpr_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred31_MathExpr() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred31_MathExpr_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred35_MathExpr() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred35_MathExpr_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}


   	protected DFA18 dfa18;
   	protected DFA21 dfa21;
   	protected DFA23 dfa23;
	private void InitializeCyclicDFAs()
	{
    	this.dfa18 = new DFA18(this);
    	this.dfa21 = new DFA21(this);
    	this.dfa23 = new DFA23(this);
	    this.dfa18.specialStateTransitionHandler = new DFA.SpecialStateTransitionHandler(DFA18_SpecialStateTransition);
	    this.dfa21.specialStateTransitionHandler = new DFA.SpecialStateTransitionHandler(DFA21_SpecialStateTransition);
	}

    const string DFA18_eotS =
        "\x17\uffff";
    const string DFA18_eofS =
        "\x01\uffff\x01\x03\x15\uffff";
    const string DFA18_minS =
        "\x02\x1c\x03\uffff\x01\x17\x03\uffff\x01\x1d\x01\x1c\x01\x17\x01"+
        "\x1d\x01\x1c\x01\x17\x01\x1d\x01\x1c\x01\x17\x01\x1d\x01\x1c\x01"+
        "\x17\x01\x1d\x01\x00";
    const string DFA18_maxS =
        "\x01\x29\x01\x37\x03\uffff\x01\x31\x03\uffff\x01\x34\x01\x33\x01"+
        "\x31\x01\x34\x01\x33\x01\x31\x01\x34\x01\x33\x01\x31\x01\x34\x01"+
        "\x33\x01\x31\x01\x34\x01\x00";
    const string DFA18_acceptS =
        "\x02\uffff\x01\x03\x01\x04\x01\x02\x01\uffff\x01\x01\x01\x05\x01"+
        "\x06\x0e\uffff";
    const string DFA18_specialS =
        "\x16\uffff\x01\x00}>";
    static readonly string[] DFA18_transitionS = {
            "\x01\x01\x0c\uffff\x01\x02",
            "\x01\x08\x0b\uffff\x01\x06\x01\x04\x06\uffff\x01\x03\x01\x07"+
            "\x01\x03\x01\x05\x03\uffff\x01\x03",
            "",
            "",
            "",
            "\x02\x06\x02\uffff\x01\x09\x01\x06\x08\uffff\x01\x06\x0b\uffff"+
            "\x01\x06",
            "",
            "",
            "",
            "\x08\x06\x01\uffff\x02\x06\x02\uffff\x06\x06\x04\uffff\x01"+
            "\x0a",
            "\x01\x08\x0b\uffff\x01\x06\x0a\uffff\x01\x0b",
            "\x02\x06\x02\uffff\x01\x0c\x01\x06\x08\uffff\x01\x06\x0b\uffff"+
            "\x01\x06",
            "\x08\x06\x01\uffff\x02\x06\x02\uffff\x06\x06\x04\uffff\x01"+
            "\x0d",
            "\x01\x08\x0b\uffff\x01\x06\x0a\uffff\x01\x0e",
            "\x02\x06\x02\uffff\x01\x0f\x01\x06\x08\uffff\x01\x06\x0b\uffff"+
            "\x01\x06",
            "\x08\x06\x01\uffff\x02\x06\x02\uffff\x06\x06\x04\uffff\x01"+
            "\x10",
            "\x01\x08\x0b\uffff\x01\x06\x0a\uffff\x01\x11",
            "\x02\x06\x02\uffff\x01\x12\x01\x06\x08\uffff\x01\x06\x0b\uffff"+
            "\x01\x06",
            "\x08\x06\x01\uffff\x02\x06\x02\uffff\x06\x06\x04\uffff\x01"+
            "\x13",
            "\x01\x08\x0b\uffff\x01\x06\x0a\uffff\x01\x14",
            "\x02\x06\x02\uffff\x01\x15\x01\x06\x08\uffff\x01\x06\x0b\uffff"+
            "\x01\x06",
            "\x08\x06\x01\uffff\x02\x06\x02\uffff\x06\x06\x04\uffff\x01"+
            "\x16",
            "\x01\uffff"
    };

    static readonly short[] DFA18_eot = DFA.UnpackEncodedString(DFA18_eotS);
    static readonly short[] DFA18_eof = DFA.UnpackEncodedString(DFA18_eofS);
    static readonly char[] DFA18_min = DFA.UnpackEncodedStringToUnsignedChars(DFA18_minS);
    static readonly char[] DFA18_max = DFA.UnpackEncodedStringToUnsignedChars(DFA18_maxS);
    static readonly short[] DFA18_accept = DFA.UnpackEncodedString(DFA18_acceptS);
    static readonly short[] DFA18_special = DFA.UnpackEncodedString(DFA18_specialS);
    static readonly short[][] DFA18_transition = DFA.UnpackEncodedStringArray(DFA18_transitionS);

    protected class DFA18 : DFA
    {
        public DFA18(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 18;
            this.eot = DFA18_eot;
            this.eof = DFA18_eof;
            this.min = DFA18_min;
            this.max = DFA18_max;
            this.accept = DFA18_accept;
            this.special = DFA18_special;
            this.transition = DFA18_transition;

        }

        override public string Description
        {
            get { return "143:1: expr0 : ( assign0 ASSIGN term | ident UNARY | UNARY ident | ident -> ^( CALL ident PARAMS ) | call | varsDecl );"; }
        }

    }


    protected internal int DFA18_SpecialStateTransition(DFA dfa, int s, IIntStream _input) //throws NoViableAltException
    {
            ITokenStream input = (ITokenStream)_input;
    	int _s = s;
        switch ( s )
        {
               	case 0 : 
                   	int LA18_22 = input.LA(1);

                   	 
                   	int index18_22 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred35_MathExpr()) ) { s = 6; }

                   	else if ( (true) ) { s = 8; }

                   	 
                   	input.Seek(index18_22);
                   	if ( s >= 0 ) return s;
                   	break;
        }
        if (state.backtracking > 0) {state.failed = true; return -1;}
        NoViableAltException nvae18 =
            new NoViableAltException(dfa.Description, 18, _s, input);
        dfa.Error(nvae18);
        throw nvae18;
    }
    const string DFA21_eotS =
        "\x0b\uffff";
    const string DFA21_eofS =
        "\x0b\uffff";
    const string DFA21_minS =
        "\x01\x0f\x01\x00\x09\uffff";
    const string DFA21_maxS =
        "\x01\x35\x01\x00\x09\uffff";
    const string DFA21_acceptS =
        "\x02\uffff\x01\x01\x01\x02\x01\x03\x01\x04\x01\x05\x01\x06\x01"+
        "\x09\x01\x07\x01\x08";
    const string DFA21_specialS =
        "\x01\uffff\x01\x00\x09\uffff}>";
    static readonly string[] DFA21_transitionS = {
            "\x01\x03\x01\uffff\x01\x05\x01\x04\x01\x06\x01\x07\x07\uffff"+
            "\x01\x01\x0c\uffff\x01\x02\x0b\uffff\x01\x08",
            "\x01\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static readonly short[] DFA21_eot = DFA.UnpackEncodedString(DFA21_eotS);
    static readonly short[] DFA21_eof = DFA.UnpackEncodedString(DFA21_eofS);
    static readonly char[] DFA21_min = DFA.UnpackEncodedStringToUnsignedChars(DFA21_minS);
    static readonly char[] DFA21_max = DFA.UnpackEncodedStringToUnsignedChars(DFA21_maxS);
    static readonly short[] DFA21_accept = DFA.UnpackEncodedString(DFA21_acceptS);
    static readonly short[] DFA21_special = DFA.UnpackEncodedString(DFA21_specialS);
    static readonly short[][] DFA21_transition = DFA.UnpackEncodedStringArray(DFA21_transitionS);

    protected class DFA21 : DFA
    {
        public DFA21(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 21;
            this.eot = DFA21_eot;
            this.eof = DFA21_eof;
            this.min = DFA21_min;
            this.max = DFA21_max;
            this.accept = DFA21_accept;
            this.special = DFA21_special;
            this.transition = DFA21_transition;

        }

        override public string Description
        {
            get { return "158:1: expr : ( expr0 ';' | IF '(' term ')' expr ( ELSE expr )? | WHILE '(' term ')' expr | FOR '(' exprList2 ';' termOrTrue ';' exprList2 ')' expr | DO expr WHILE '(' term ')' | RETURN term ';' | call ';' | callArray ';' | blockExpr );"; }
        }

    }


    protected internal int DFA21_SpecialStateTransition(DFA dfa, int s, IIntStream _input) //throws NoViableAltException
    {
            ITokenStream input = (ITokenStream)_input;
    	int _s = s;
        switch ( s )
        {
               	case 0 : 
                   	int LA21_1 = input.LA(1);

                   	 
                   	int index21_1 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred41_MathExpr()) ) { s = 2; }

                   	else if ( (synpred48_MathExpr()) ) { s = 9; }

                   	else if ( (synpred49_MathExpr()) ) { s = 10; }

                   	 
                   	input.Seek(index21_1);
                   	if ( s >= 0 ) return s;
                   	break;
        }
        if (state.backtracking > 0) {state.failed = true; return -1;}
        NoViableAltException nvae21 =
            new NoViableAltException(dfa.Description, 21, _s, input);
        dfa.Error(nvae21);
        throw nvae21;
    }
    const string DFA23_eotS =
        "\x04\uffff";
    const string DFA23_eofS =
        "\x04\uffff";
    const string DFA23_minS =
        "\x02\x0f\x02\uffff";
    const string DFA23_maxS =
        "\x02\x37\x02\uffff";
    const string DFA23_acceptS =
        "\x02\uffff\x01\x02\x01\x01";
    const string DFA23_specialS =
        "\x04\uffff}>";
    static readonly string[] DFA23_transitionS = {
            "\x01\x03\x01\uffff\x04\x03\x07\uffff\x01\x03\x0c\uffff\x01"+
            "\x03\x0b\uffff\x01\x03\x01\x02\x01\x01",
            "\x01\x03\x01\uffff\x04\x03\x07\uffff\x01\x03\x0c\uffff\x01"+
            "\x03\x0b\uffff\x01\x03\x01\x02\x01\x01",
            "",
            ""
    };

    static readonly short[] DFA23_eot = DFA.UnpackEncodedString(DFA23_eotS);
    static readonly short[] DFA23_eof = DFA.UnpackEncodedString(DFA23_eofS);
    static readonly char[] DFA23_min = DFA.UnpackEncodedStringToUnsignedChars(DFA23_minS);
    static readonly char[] DFA23_max = DFA.UnpackEncodedStringToUnsignedChars(DFA23_maxS);
    static readonly short[] DFA23_accept = DFA.UnpackEncodedString(DFA23_acceptS);
    static readonly short[] DFA23_special = DFA.UnpackEncodedString(DFA23_specialS);
    static readonly short[][] DFA23_transition = DFA.UnpackEncodedStringArray(DFA23_transitionS);

    protected class DFA23 : DFA
    {
        public DFA23(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 23;
            this.eot = DFA23_eot;
            this.eof = DFA23_eof;
            this.min = DFA23_min;
            this.max = DFA23_max;
            this.accept = DFA23_accept;
            this.special = DFA23_special;
            this.transition = DFA23_transition;

        }

        override public string Description
        {
            get { return "()* loopback of 170:18: ( ( ';' )* expr )*"; }
        }

    }

 

    public static readonly BitSet FOLLOW_IDENT_in_ident853 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENT_in_type860 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_term_in_params_870 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_48_in_params_873 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_term_in_params_876 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_ident_in_call889 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_49_in_call891 = new BitSet(new ulong[]{0x0006002019800000UL});
    public static readonly BitSet FOLLOW_params__in_call893 = new BitSet(new ulong[]{0x0004000000000000UL});
    public static readonly BitSet FOLLOW_50_in_call895 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ident_in_callArray920 = new BitSet(new ulong[]{0x0008000000000000UL});
    public static readonly BitSet FOLLOW_arrayIndex_in_callArray922 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_49_in_group943 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_term_in_group946 = new BitSet(new ulong[]{0x0004000000000000UL});
    public static readonly BitSet FOLLOW_50_in_group948 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NUMBER_in_group953 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_TRUE_in_group957 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_FALSE_in_group961 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ident_in_group965 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_call_in_group969 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_callArray_in_group973 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_group_in_not983 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NOT_in_not987 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_not_in_not990 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_not_in_mult998 = new BitSet(new ulong[]{0x0000004780000002UL});
    public static readonly BitSet FOLLOW_set_in_mult1002 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_not_in_mult1025 = new BitSet(new ulong[]{0x0000004780000002UL});
    public static readonly BitSet FOLLOW_mult_in_add1038 = new BitSet(new ulong[]{0x0000008060000002UL});
    public static readonly BitSet FOLLOW_set_in_add1043 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_mult_in_add1058 = new BitSet(new ulong[]{0x0000008060000002UL});
    public static readonly BitSet FOLLOW_add_in_compare1087 = new BitSet(new ulong[]{0x0000FC0000000002UL});
    public static readonly BitSet FOLLOW_set_in_compare1091 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_add_in_compare1118 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_compare_in_and_logic1130 = new BitSet(new ulong[]{0x0000000800000002UL});
    public static readonly BitSet FOLLOW_AND_in_and_logic1134 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_compare_in_and_logic1137 = new BitSet(new ulong[]{0x0000000800000002UL});
    public static readonly BitSet FOLLOW_and_logic_in_or_logic1150 = new BitSet(new ulong[]{0x0000001000000002UL});
    public static readonly BitSet FOLLOW_OR_in_or_logic1154 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_and_logic_in_or_logic1157 = new BitSet(new ulong[]{0x0000001000000002UL});
    public static readonly BitSet FOLLOW_or_logic_in_term1168 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_51_in_arrayConst1178 = new BitSet(new ulong[]{0x0000000008000000UL});
    public static readonly BitSet FOLLOW_NUMBER_in_arrayConst1181 = new BitSet(new ulong[]{0x0010000000000000UL});
    public static readonly BitSet FOLLOW_52_in_arrayConst1183 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_51_in_arrayConst1188 = new BitSet(new ulong[]{0x0000000008000000UL});
    public static readonly BitSet FOLLOW_NUMBER_in_arrayConst1191 = new BitSet(new ulong[]{0x0010000000000000UL});
    public static readonly BitSet FOLLOW_52_in_arrayConst1193 = new BitSet(new ulong[]{0x0008000000000000UL});
    public static readonly BitSet FOLLOW_arrayConst_in_arrayConst1196 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NUMBER_in_arrayNumber1203 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NUMBER_in_arrayNumber1207 = new BitSet(new ulong[]{0x0001000000000000UL});
    public static readonly BitSet FOLLOW_48_in_arrayNumber1209 = new BitSet(new ulong[]{0x0000000008000000UL});
    public static readonly BitSet FOLLOW_arrayNumber_in_arrayNumber1212 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_53_in_arrayInit1218 = new BitSet(new ulong[]{0x0000000008000000UL});
    public static readonly BitSet FOLLOW_arrayNumber_in_arrayInit1221 = new BitSet(new ulong[]{0x0040000000000000UL});
    public static readonly BitSet FOLLOW_54_in_arrayInit1223 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ident_in_varDecl1231 = new BitSet(new ulong[]{0x0000010000000002UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_varDecl1234 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_term_in_varDecl1237 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_type_in_varsDecl1246 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_varDecl_in_varsDecl1248 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_48_in_varsDecl1252 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_varDecl_in_varsDecl1254 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_type_in_varsDecl1283 = new BitSet(new ulong[]{0x0008000000000000UL});
    public static readonly BitSet FOLLOW_arrayConst_in_varsDecl1285 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_varDecl_in_varsDecl1287 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_48_in_varsDecl1291 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_varDecl_in_varsDecl1293 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_type_in_varsDecl1317 = new BitSet(new ulong[]{0x0008000000000000UL});
    public static readonly BitSet FOLLOW_arrayConst_in_varsDecl1319 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_ident_in_varsDecl1321 = new BitSet(new ulong[]{0x0000010000000000UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_varsDecl1323 = new BitSet(new ulong[]{0x0020000000000000UL});
    public static readonly BitSet FOLLOW_arrayInit_in_varsDecl1325 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_51_in_arrayIndex1361 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_term_in_arrayIndex1364 = new BitSet(new ulong[]{0x0010000000000000UL});
    public static readonly BitSet FOLLOW_52_in_arrayIndex1366 = new BitSet(new ulong[]{0x0008000000000002UL});
    public static readonly BitSet FOLLOW_ident_in_assign01378 = new BitSet(new ulong[]{0x0008000000000000UL});
    public static readonly BitSet FOLLOW_arrayIndex_in_assign01380 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ident_in_assign01398 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_assign0_in_expr01408 = new BitSet(new ulong[]{0x0000010000000000UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_expr01410 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_term_in_expr01413 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ident_in_expr01417 = new BitSet(new ulong[]{0x0000020000000000UL});
    public static readonly BitSet FOLLOW_UNARY_in_expr01419 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_UNARY_in_expr01424 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_ident_in_expr01427 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ident_in_expr01431 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_call_in_expr01445 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_varsDecl_in_expr01449 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_53_in_blockExpr1456 = new BitSet(new ulong[]{0x00E20200199E8000UL});
    public static readonly BitSet FOLLOW_exprList_in_blockExpr1459 = new BitSet(new ulong[]{0x0040000000000000UL});
    public static readonly BitSet FOLLOW_54_in_blockExpr1461 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_term_in_termOrTrue1473 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expr0_in_expr1493 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_55_in_expr1495 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IF_in_expr1500 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_49_in_expr1503 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_term_in_expr1506 = new BitSet(new ulong[]{0x0004000000000000UL});
    public static readonly BitSet FOLLOW_50_in_expr1508 = new BitSet(new ulong[]{0x00220200199E8000UL});
    public static readonly BitSet FOLLOW_expr_in_expr1511 = new BitSet(new ulong[]{0x0000000000010002UL});
    public static readonly BitSet FOLLOW_ELSE_in_expr1514 = new BitSet(new ulong[]{0x00220200199E8000UL});
    public static readonly BitSet FOLLOW_expr_in_expr1517 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_WHILE_in_expr1523 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_49_in_expr1526 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_term_in_expr1529 = new BitSet(new ulong[]{0x0004000000000000UL});
    public static readonly BitSet FOLLOW_50_in_expr1531 = new BitSet(new ulong[]{0x00220200199E8000UL});
    public static readonly BitSet FOLLOW_expr_in_expr1534 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_FOR_in_expr1538 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_49_in_expr1541 = new BitSet(new ulong[]{0x0080020010000000UL});
    public static readonly BitSet FOLLOW_exprList2_in_expr1544 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_55_in_expr1546 = new BitSet(new ulong[]{0x0082002019800000UL});
    public static readonly BitSet FOLLOW_termOrTrue_in_expr1549 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_55_in_expr1551 = new BitSet(new ulong[]{0x0004020010000000UL});
    public static readonly BitSet FOLLOW_exprList2_in_expr1554 = new BitSet(new ulong[]{0x0004000000000000UL});
    public static readonly BitSet FOLLOW_50_in_expr1556 = new BitSet(new ulong[]{0x00220200199E8000UL});
    public static readonly BitSet FOLLOW_expr_in_expr1559 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_DO_in_expr1563 = new BitSet(new ulong[]{0x00220200199E8000UL});
    public static readonly BitSet FOLLOW_expr_in_expr1566 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_WHILE_in_expr1568 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_49_in_expr1571 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_term_in_expr1574 = new BitSet(new ulong[]{0x0004000000000000UL});
    public static readonly BitSet FOLLOW_50_in_expr1576 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_RETURN_in_expr1581 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_term_in_expr1584 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_55_in_expr1586 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_call_in_expr1591 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_55_in_expr1593 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_callArray_in_expr1598 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_55_in_expr1600 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_blockExpr_in_expr1605 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expr_in_exprList1617 = new BitSet(new ulong[]{0x00A20200199E8002UL});
    public static readonly BitSet FOLLOW_55_in_exprList1621 = new BitSet(new ulong[]{0x00A20200199E8000UL});
    public static readonly BitSet FOLLOW_expr_in_exprList1624 = new BitSet(new ulong[]{0x00A20200199E8002UL});
    public static readonly BitSet FOLLOW_55_in_exprList1632 = new BitSet(new ulong[]{0x0080000000000002UL});
    public static readonly BitSet FOLLOW_expr0_in_exprList21655 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_48_in_exprList21659 = new BitSet(new ulong[]{0x0000020010000000UL});
    public static readonly BitSet FOLLOW_expr0_in_exprList21661 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_type_in_paramDecl1687 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_IDENT_in_paramDecl1689 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_paramDecl_in_paramsDecl1698 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_48_in_paramsDecl1702 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_paramDecl_in_paramsDecl1705 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_type_in_funcDecl1720 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_ident_in_funcDecl1724 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_49_in_funcDecl1726 = new BitSet(new ulong[]{0x0004000010000000UL});
    public static readonly BitSet FOLLOW_paramsDecl_in_funcDecl1728 = new BitSet(new ulong[]{0x0004000000000000UL});
    public static readonly BitSet FOLLOW_50_in_funcDecl1731 = new BitSet(new ulong[]{0x0020000000000000UL});
    public static readonly BitSet FOLLOW_53_in_funcDecl1735 = new BitSet(new ulong[]{0x00E20200199E8000UL});
    public static readonly BitSet FOLLOW_exprList_in_funcDecl1737 = new BitSet(new ulong[]{0x0040000000000000UL});
    public static readonly BitSet FOLLOW_54_in_funcDecl1739 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_funcDecl_in_exprOrFuncDecl1770 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expr_in_exprOrFuncDecl1774 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_exprOrFuncDecl_in_program1784 = new BitSet(new ulong[]{0x00A20200199E8002UL});
    public static readonly BitSet FOLLOW_55_in_program1786 = new BitSet(new ulong[]{0x00A20200199E8002UL});
    public static readonly BitSet FOLLOW_program_in_result1799 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_result_in_execute1817 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_48_in_synpred29_MathExpr1252 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_varDecl_in_synpred29_MathExpr1254 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_type_in_synpred30_MathExpr1246 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_varDecl_in_synpred30_MathExpr1248 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_48_in_synpred30_MathExpr1252 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_varDecl_in_synpred30_MathExpr1254 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_48_in_synpred31_MathExpr1291 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_varDecl_in_synpred31_MathExpr1293 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_type_in_synpred32_MathExpr1283 = new BitSet(new ulong[]{0x0008000000000000UL});
    public static readonly BitSet FOLLOW_arrayConst_in_synpred32_MathExpr1285 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_varDecl_in_synpred32_MathExpr1287 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_48_in_synpred32_MathExpr1291 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_varDecl_in_synpred32_MathExpr1293 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_assign0_in_synpred35_MathExpr1408 = new BitSet(new ulong[]{0x0000010000000000UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_synpred35_MathExpr1410 = new BitSet(new ulong[]{0x0002002019800000UL});
    public static readonly BitSet FOLLOW_term_in_synpred35_MathExpr1413 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expr0_in_synpred41_MathExpr1493 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_55_in_synpred41_MathExpr1495 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ELSE_in_synpred42_MathExpr1514 = new BitSet(new ulong[]{0x00220200199E8000UL});
    public static readonly BitSet FOLLOW_expr_in_synpred42_MathExpr1517 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_call_in_synpred48_MathExpr1591 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_55_in_synpred48_MathExpr1593 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_callArray_in_synpred49_MathExpr1598 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_55_in_synpred49_MathExpr1600 = new BitSet(new ulong[]{0x0000000000000002UL});

}
}