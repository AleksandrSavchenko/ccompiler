void main()
{
	int[10] m = {10, 8, 3, 4, 6, 7, 9, 1, 0, 2};
	
	int tmp;
	for (int i=0; i<10; i++)
	   for (int j=0; j<10; j++)
	     if (m[j] > m[j+1])
		 {
			tmp = m[j];
			m[j] = m[j+1];
			m[j+1] = tmp;
		 }

	for (int z=0; z<10; z++)
	printf(m[z]);
}
