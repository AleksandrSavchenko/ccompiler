﻿using System;
using System.Collections.Generic;

namespace MathExpr
{
    public static class TypeConverter
    {
        public static String Convert(String type)
        {
            switch(type)
            {
                case "int": return "int32";
                case "int32": return "int32";
                case "void": return "void";
                default: throw new MSILGeneratorException("Не известный тип для MSIL генератора: "+type);
            }
        }
    }
}
