using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

using Antlr.Runtime.Tree;

using AstNodeType = MathExpr.MathExprParser;


namespace MathExpr
{
    public class MSILGenerator
    {
        private ITree programNode = null;
        private Dictionary<string, int> vars = new Dictionary<string, int>();
        private Dictionary<string, int> fargs = new Dictionary<string, int>();
        private Dictionary<string, FunctionType> functions = new Dictionary<string, FunctionType>();
        private StringBuilder msil = new StringBuilder();
        private int labIndex = 0;
        private DefaultFunctions defaultFunctions = DefaultFunctions.INSTANCE;


        public MSILGenerator(ITree programNode)
        {
            if (programNode.Type != AstNodeType.PROGRAM)
                throw new MSILGeneratorException("AST-������ �� �������� ����������");

            this.programNode = programNode;
        }
                
        private void Generate(ITree node)
        {
            int tempLabIndex;

            switch (node.Type)
            {
                case AstNodeType.ASSIGN:
                    String indexVar = "";
                    if (node.GetChild(0).Type == AstNodeType.ARRAY_SET)
                    {
                        Generate(node.GetChild(0));
                        Generate(node.GetChild(1));
                        indexVar = SearchVar(node.GetChild(0));
                        msil.Append(string.Format("    stelem.i4\n"));
                    }
                    else
                    if (node.GetChild(0).Type == AstNodeType.VAR_ARRAY)
                    {
                        Generate(node.GetChild(0));
                        Generate(node.GetChild(1));
                    }
                    else
                    {
                        Generate(node.GetChild(1));
                        indexVar = SearchVar(node.GetChild(0));
                        msil.Append(string.Format("    stloc.s {0}\n", vars[indexVar]));
                    }

                    break;

                case AstNodeType.IDENT:
                    if (fargs.ContainsKey(node.Text))
                    {
                        msil.Append(string.Format("    ldarg.s {0}\n", fargs[node.Text]));
                    }
                    else
                    {
                        msil.Append(string.Format("    ldloc.s {0}\n", vars[node.Text]));
                    }
                    break;

                case AstNodeType.NUMBER:
                    msil.Append(string.Format("    ldc.i4.s {0}\n", node.Text));
                    break;

                case AstNodeType.CALL:

                    String funName = node.GetChild(0).Text;
                    // ����� ��������� �������
                    if (defaultFunctions.isDefault(funName))
                    {
                        if (node.GetChild(1).ChildCount > 0)
                        {
                            Generate(node.GetChild(1).GetChild(0));
                        }
                        FunctionType fType = defaultFunctions.search(funName);
                        msil.Append(string.Format("    call {0}\n", fType.ToString()));
                        if (fType.IsPopType())
                        {
                            //msil.Append(string.Format("    pop\n"));
                        }
                    }
                    else //����� ���� ������� �� ���������
                    {
                        FunctionType func = functions[node.GetChild(0).Text];
                        Generate(node.GetChild(1));
                        msil.Append(string.Format("    call {0} {1}{2}({3})\n", func.Type, func.SystemName, func.Name, func.argsToString()));
                    }
                    break;

                case AstNodeType.ADD:
                case AstNodeType.SUB:
                case AstNodeType.MUL:
                case AstNodeType.DIV:
                case AstNodeType.MOD:
                    string oper = node.Type == AstNodeType.ADD ? "add" :
                                  node.Type == AstNodeType.SUB ? "sub" :
                                  node.Type == AstNodeType.MUL ? "mul" :
                                  node.Type == AstNodeType.DIV ? "div" :
                                  node.Type == AstNodeType.MOD ? "rem" :
                                  "unknown";

                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    {0}\n", oper));
                    break;

                case AstNodeType.IF:
                    tempLabIndex = labIndex;
                    labIndex += 2;
                    Generate(node.GetChild(0));
                    msil.Append(string.Format("    brfalse.s L_{0:X4}\n", tempLabIndex + 1));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    br.s L_{0:X4}\n", tempLabIndex + 2));
                    msil.Append(string.Format("  L_{0:X4}:\n", tempLabIndex + 1));
                    if (node.ChildCount > 2)
                        Generate(node.GetChild(2));
                    msil.Append(string.Format("  L_{0:X4}:\n", tempLabIndex + 2));
                    break;
                    
                case AstNodeType.ELSE:
                    throw new MSILGeneratorException("Not Realize = " + node.Text);

                case AstNodeType.WHILE:
                    tempLabIndex = labIndex;
                    labIndex += 2;
                    msil.Append(string.Format("  L_{0:X4}:\n", tempLabIndex + 1));
                    Generate(node.GetChild(0));
                    msil.Append(string.Format("    brfalse.s L_{0:X4}\n", tempLabIndex + 2));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    br.s L_{0:X4}\n", tempLabIndex + 1));
                    msil.Append(string.Format("  L_{0:X4}:\n", tempLabIndex + 2));
                    break;

                case AstNodeType.FOR:
                    tempLabIndex = labIndex;
                    labIndex += 2;
                    Generate(node.GetChild(0));
                    indexVar = SearchVar(node.GetChild(0));

                    msil.Append(string.Format("    ldc.i4.1\n"));
                    msil.Append(string.Format("    brtrue.s L_{0:X4}\n", tempLabIndex + 2));
                    msil.Append(string.Format("  L_{0:X4}:\n", tempLabIndex + 1));
                    Generate(node.GetChild(3));
                    Generate(node.GetChild(2));
                    msil.Append(string.Format("  L_{0:X4}:\n", tempLabIndex + 2));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    brtrue.s L_{0:X4}\n", tempLabIndex + 1));
                    break;

                case AstNodeType.DO:
                    tempLabIndex = labIndex;
                    labIndex += 1;
                    msil.Append(string.Format("  L_{0:X4}:\n", tempLabIndex + 1));
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    brtrue.s L_{0:X4}\n", tempLabIndex + 1));
                    break;

                case AstNodeType.BLOCK:
                case AstNodeType.PROGRAM:
                    for (int i = 0; i < node.ChildCount; i++)
                        Generate(node.GetChild(i));
                    break;


                case AstNodeType.VAR:
                    for (int i = 0; i < node.GetChild(0).ChildCount; i++)
                        Generate(node.GetChild(0).GetChild(i));
                    break;


                //����������
                case AstNodeType.AND:
                    Generate(node.GetChild(0));
                    msil.Append(string.Format("    brfalse.s\n"));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    br.s\n"));
                    break;
                case AstNodeType.OR:
                    Generate(node.GetChild(0));
                    msil.Append(string.Format("    brtrue.s\n"));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    br.s\n"));
                    break;
                case AstNodeType.NOT:
                    Generate(node.GetChild(0));
                    msil.Append(string.Format("    ceq\n"));
                    break;

                // ������� �������� '++' & '--'
                case AstNodeType.UNARY:
                    switch(node.Text)
                    {
                        case "++":
                            String identA = node.GetChild(0).Text;
                            Generate(node.GetChild(0));
                            msil.Append(string.Format("    ldc.i4.1\n"));
                            msil.Append(string.Format("    add\n"));
                            msil.Append(string.Format("    stloc.s {0}\n", vars[identA]));
                            break;
                        case "--":
                            String identD = node.GetChild(0).Text;
                            msil.Append(string.Format("    ldc.i4.1\n"));
                            msil.Append(string.Format("    sub\n"));
                            msil.Append(string.Format("    stloc.s {0}\n", vars[identD]));
                            break;
                        default: throw new MSILGeneratorException("�� ��������� ������� �������� " + node.Text);
                    }
                    break;

                //���������
                // '>='
                case AstNodeType.GE:
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    clt\n"));
                    msil.Append(string.Format("    ldc.i4.0\n"));
                    msil.Append(string.Format("    ceq\n"));
                    break;
                // '>'
                case AstNodeType.GT:
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    cgt\n"));
                    break;
                // '<='
                case AstNodeType.LE:
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    cgt\n"));
                    msil.Append(string.Format("    ldc.i4.0\n"));
                    msil.Append(string.Format("    ceq\n"));
                    break;
                // '<'
                case AstNodeType.LT:
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    clt\n"));
                    break;
                // '!='
                case AstNodeType.NEQUALS:
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    ceq\n"));
                    msil.Append(string.Format("    ldc.i4.0\n"));
                    msil.Append(string.Format("    ceq\n"));
                    break;
                // '=='
                case AstNodeType.EQUALS:
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    ceq\n"));
                    break;

                //���������
                case AstNodeType.XOR:
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    xor\n"));
                    break;
                case AstNodeType.BIT_AND:
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    and\n"));
                    break;
                case AstNodeType.BIT_OR:
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    or\n"));
                    break;

                //��������� ���������
                case AstNodeType.TRUE:
                    msil.Append(string.Format("    ldc.i4.1\n"));
                    break;
                case AstNodeType.FALSE:
                    msil.Append(string.Format("    ldc.i4.0\n"));
                    break;

                // Return
                case AstNodeType.RETURN:
                    Generate(node.GetChild(0));
                    msil.Append(string.Format("    ret\n"));
                    break;

                //����������� �������
                case AstNodeType.FUNCTION:
                    vars.Clear();
                    String type = TypeConverter.Convert(node.GetChild(0).Text);
                    String name = node.GetChild(1).Text;
                    name = name.ToLower().Equals("main") ? "Main" : name;

                    Generate(node.GetChild(2)); // ����������� ���������� �������
                    Variable[] arguments = GetFunctionParameters(node.GetChild(2));

                    msil.Append(string.Format("\n.method public static {0} {1}({2}) cil managed\n", type, name, FuncParamToString(arguments)));
                    functions.Add(name, new FunctionType(name, "Program::", type, arguments));
                    msil.Append("{\n");
                    CheckOnEntryPoint(msil, type, name);
                    AddMaxStack(msil);

                    // ����� ��������� ���������� � ��������� �������
                    List<Variable> localVars = SearchLocalVars(node);
                    AddToDictinary(vars, localVars);
                    LocalInit(localVars, msil);

                    // ����� ���� ���������� ������ (�����)
                    Generate(node.GetChild(3));

                    // ����� return ��� ����� ����� � return;
                    // ���� ���� ��������� ������� � ��� Return �� ������� ���, ���� ��� �� ������ msil.Append(string.Format("    ret\n"));
                    if (!IsReturnExist(node.GetChild(3)))
                    {
                        msil.Append(string.Format("    ret\n"));
                    }
                    msil.Append("}\n");
                    break;

                case AstNodeType.PARAMS:
                    for (int i = 0; i < node.ChildCount; i++)
                    {
                        Generate(node.GetChild(i));
                    }
                    break;

                case AstNodeType.ARGUMENTS:
                    for (int i = 0; i < node.ChildCount; i++)
                    {
                        fargs.Add(node.GetChild(i).Text, i);
                    }
                    break;

                //��������� � �������� �������
                case AstNodeType.ARRAY_CALL:
                    msil.Append(string.Format("    ldloc.s {0}\n", vars[node.GetChild(0).Text]));
                    Generate(node.GetChild(1));
                    msil.Append(string.Format("    ldelem.i4\n"));
                    break;
                
                //����������� ��������
                case AstNodeType.ARRAY_INDEX:
                    for (int i = 0; i < node.ChildCount; i++)
                    {
                        Generate(node.GetChild(i));
                    }
                    break;
                
                // ������������� �������
                case AstNodeType.ARRAY_INIT:
                    String ident = node.Parent.GetChild(0).GetChild(1).Text;
                    for (int i=0; i<node.ChildCount; i++)
                    {
                        msil.Append(string.Format("    ldloc.s {0}\n", vars[ident]));
                        msil.Append(string.Format("    ldc.i4.s {0}\n", i));
                        msil.Append(string.Format("    ldc.i4.s {0}\n", node.GetChild(i).Text));
                        msil.Append(string.Format("    stelem.i4\n"));
                    }
                    break;

                // ���������� �������� �������� �������
                case AstNodeType.ARRAY_SET:
                    //msil.Append(string.Format("    ldloc.s {0}\n", vars[node.Text]));
                    Generate(node.GetChild(0));
                    Generate(node.GetChild(1));
                    break;

                // ���������� �������
                case AstNodeType.VAR_ARRAY:
                    ITree varNode = node.GetChild(0);
                    for (int i=0; i< varNode.ChildCount; i++)
                    {
                        Generate(varNode.GetChild(i));
                    }
                    msil.Append(string.Format("    newarr [mscorlib]System.Int32\n"));
                    msil.Append(string.Format("    stloc.s {0}\n", vars[node.GetChild(1).Text]));
                    break;

                default:
                    throw new MSILGeneratorException("Not implemented!");
            }
        }


        private void Generate()
        {
            msil = new StringBuilder();
            labIndex = 0;

            msil.Append(".assembly program");
            msil.Append("\n{");
            msil.Append("\n}");

            msil.Append("\n.class public Program");
            msil.Append("\n     extends[mscorlib]System.Object");
            msil.Append("\n{");
            Generate(programNode);
            msil.Append("\n}");
        }


        public static string GenerateMSIL(ITree programNode)
        {
            MSILGenerator g = null;
            try {
                g = new MSILGenerator(programNode);
                g.Generate();
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return g.msil.ToString();
        }

        public static Variable[] GetFunctionParameters(ITree node)
        {
            Variable[] arguments = new Variable[node.ChildCount];
            for (int i = 0; i < node.ChildCount; i++)
            {
                arguments[i] = new Variable(node.GetChild(i).Text, node.GetChild(i).GetChild(0).Text);
            }
            return arguments;
        }

        public static String FuncParamToString(Variable[] parameters)
        {
            String str = String.Empty;
            for (int i=0; i<parameters.Length; i++)
            {
                str += parameters[i].Type + (i + 1 < parameters.Length ? ", " : "");
             }
            return str;
        }

        public static void LocalInit(List<Variable> localVars, StringBuilder msil)
        {
            msil.Append("    .locals init (\n");
            int index = 0;
            foreach (Variable lvar in localVars)
                msil.Append(string.Format("      [{0}] {1} {2}{3}\n", index, lvar.Type, lvar.Name, ++index < localVars.Count ? "," : ""));
            msil.Append("    )\n");
        }

        public static void CheckOnEntryPoint(StringBuilder msil, String type, String name)
        {
            if (type.ToLower() == "void" && name.ToLower() == "main")
                msil.Append("    .entrypoint\n");
        }

        public static bool IsReturnExist(ITree node)
        {
            if (node.GetChild(node.ChildCount - 1).Type == AstNodeType.RETURN)
                return true;
            return false;
        }

        public List<Variable> SearchLocalVars(ITree node)
        {
            List<Variable> localVars = new List<Variable>();
            StartSearchLocalVars(node, localVars);
            return localVars;
        }
        public void StartSearchLocalVars(ITree node, List<Variable> localVars)
        {
            if (node.Type == AstNodeType.VAR || node.Type == AstNodeType.VAR_ARRAY)
            {
                //���� ��� IDENT-� � ���������
                HashSet<String> set = SearchIdentForLocalVars(node);
                AddToLocalVars(localVars, "int", set);
            }
            for (int i = 0; i < node.ChildCount; i++)
                StartSearchLocalVars(node.GetChild(i), localVars);
        }

        public static HashSet<String> SearchIdentForLocalVars(ITree node)
        {
            HashSet<String> set = new HashSet<String>();
            if (node.Type == AstNodeType.IDENT && node.Text != "int" 
                && node.Parent.Type != AstNodeType.CALL && node.Parent.Type != AstNodeType.FUNCTION)
            {
                set.Add(node.Text);
                return set;
            }
            for (int i = 0; i < node.ChildCount; i++)
                foreach (var el in SearchIdentForLocalVars(node.GetChild(i)))
                {
                    set.Add(el);
                }
            return set;
        }

        public static void StartSearchIdentForLocalVars(ITree node, HashSet<String> set)
        {
            if (node.Type == AstNodeType.IDENT && node.Text != "int" && node.Parent.Type != AstNodeType.ARGUMENTS)
            {
                set.Add(node.Text);
            }
            for (int i = 0; i < node.ChildCount; i++)
                StartSearchIdentForLocalVars(node.GetChild(i), set);
        }

        private void AddToLocalVars(List<Variable> localVars, String type, HashSet<String> idents)
        {
            foreach (String ident in idents)
            {
                Variable v = new Variable(ident, type);
                if (!localVars.Contains(v) && !fargs.ContainsKey(v.Name))
                {
                    localVars.Add(v);
                }
            }
        }

        private static void AddToDictinary(Dictionary<string, int> dictinary, List<Variable> listVar)
        {
            for (int i=0; i<listVar.Count; i++)
            {
                dictinary.Add(listVar[i].Name, i);
            }
        }

        private String SearchVar(ITree node)
        {
            if (node.Type == AstNodeType.IDENT)
            {
                if (vars.ContainsKey(node.Text))
                {
                    return node.Text;
                }
            }
            for (int i = 0; i < node.ChildCount; i++)
            {
                String val = SearchVar(node.GetChild(i));
                if (val != null)
                    return val;
            }
            return null;
        }

        private void AddMaxStack(StringBuilder msil)
        {
            msil.Append("\n    .maxstack 8\n");
        }
    }
}
